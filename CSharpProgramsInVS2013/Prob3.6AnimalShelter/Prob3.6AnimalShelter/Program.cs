﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._6AnimalShelter
{
    class Program
    {
        static void Main(string[] args)
        {
            var testAnimalShelter = new AnimalShelter();
            LinkedListNode<Animal> currentNode;

            var animal1 = new Dog("Dog1");
            var animal2 = new Dog("Dog2");
            var animal3 = new Cat("Cat1");
            var animal4 = new Cat("Cat2");
            var animal5 = new Dog("Dog3");
            var animal6 = new Cat("Cat3");
            var animal7 = new Cat("Cat4");
            var animal8 = new Animal("Animal8, actually first of type Animal, not Cat or Dog");
            var animal9 = new Animal("Dog4");

            testAnimalShelter.Enqueue(animal1);
            testAnimalShelter.Enqueue(animal2);
            testAnimalShelter.Enqueue(animal3);
            testAnimalShelter.Enqueue(animal4);
            testAnimalShelter.Enqueue(animal5);
            testAnimalShelter.Enqueue(animal6);
            testAnimalShelter.Enqueue(animal7);
            testAnimalShelter.Enqueue(animal8);
            testAnimalShelter.Enqueue(animal9);

            Console.WriteLine("Initial animalQueue:");
            currentNode = testAnimalShelter.animalQueue.First;
            while (currentNode != null)
            {
                Console.WriteLine("{0}", currentNode.Value.name);
                currentNode = currentNode.Next;
            }

            Console.WriteLine("First animal in queue: {0}", testAnimalShelter.DequeueAny().name);
            Console.WriteLine("First cat in queue: {0}", testAnimalShelter.DequeueCat().name);
            Console.WriteLine("First dog in queue: {0}", testAnimalShelter.DequeueDog().name);
            Console.ReadLine();
        }
    }
}
