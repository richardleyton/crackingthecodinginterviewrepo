﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._6AnimalShelter
{
    public class AnimalShelter
    {
        public LinkedList<Animal> animalQueue = new LinkedList<Animal>();

        public void Enqueue(Animal item)
        {
            animalQueue.AddLast(item);
        }

        public Animal DequeueAny()
        {
            Animal returnValue;
            if (animalQueue != null && animalQueue.Count != 0)
            {
                returnValue = animalQueue.First.Value;
                animalQueue.RemoveFirst();
                return returnValue;
            }
                return null;
        }

        public Dog DequeueDog()
        {
            return (Dog)DequeueDogOrCat(typeof(Dog));  // The cast is necessary
        }

        public Cat DequeueCat()
        {
            return (Cat)DequeueDogOrCat(typeof(Cat));       // The cast is necessary
        }

        // Helper function for Dog and Cat
        public Animal DequeueDogOrCat(Type dogOrCatType)
        {
            LinkedListNode<Animal> currentNode;
            Animal returnValue;
            if (animalQueue != null && animalQueue.Count != 0)
            {

                currentNode = animalQueue.First;
                while (currentNode != null)
                {
                    //Type typeForDebugging = currentNode.Value.GetType();  //Debugging
                    
                    //if (typeof(currentNode.Value) == dogOrCatType)
                    //if (currentNode.Value is dogOrCatType)
                    if (currentNode.Value.GetType() == dogOrCatType)
                    {
                        returnValue = currentNode.Value;
                        animalQueue.Remove(currentNode);
                        return returnValue;
                    }
                    currentNode = currentNode.Next;
                }
            }           
            return null;
        }
    }
}
