﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace Prob1._3StringPermutationCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            string testStringA = null;
            string testStringB = null;
            Debug.Assert(!testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("null test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            testStringA = "";
            testStringB = "";
            Debug.Assert(!testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("Empty string test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            testStringA = "aaaa";
            testStringB = "aaaaa";
            Debug.Assert(!testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("Different length string test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            testStringA = "abcdefg";
            testStringB = "abcdefg";
            Debug.Assert(testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("Identical string test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            testStringA = "abcdefg";
            testStringB = "dcbgfae";
            Debug.Assert(testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("Same letters, different permutation string test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            testStringA = "abcddefg";
            testStringB = "dcabgfae";
            Debug.Assert(!testStringA.IsStringPermutationExtensionMethod(testStringB));
            Console.WriteLine("Some different letters, string test");
            Console.WriteLine("{0} is {1} permutation of {2}\n", testStringA,
                testStringA.IsStringPermutationExtensionMethod(testStringB) ? "a" : "not a", testStringB);

            Console.ReadLine();

        }
    }
}
