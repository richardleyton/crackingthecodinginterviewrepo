﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._3StringPermutationCheck
{
    public static class ExtensionMethods
    {
        public static bool IsStringPermutationExtensionMethod(this string string1, string string2)
        {
            if (String.IsNullOrEmpty(string1) || String.IsNullOrEmpty(string2) || string1.Length != string2.Length)
            {
                return false;
            }
            // Sort both strings and compare.  If they are equal, one is the permutation of the other.
            string string1WithOrderedChars = new string(string1.OrderBy(c => c).ToArray());
            string string2WithOrderedChars = new string(string2.OrderBy(c => c).ToArray());
            if (string1WithOrderedChars == string2WithOrderedChars)
            {
                return true;
            }
            return false;
        }
    }
}
