﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._8LoopDetection
{
    public class LinkedListNodeWithoutCircularRestrictions<T>
    {
        public T Value;
        public LinkedListNodeWithoutCircularRestrictions<T> Next;
    }
    
    public class LinkedListWithoutCircularRestrictions<T>
    {
        public LinkedListNodeWithoutCircularRestrictions<T> First { get; set; }
        public LinkedListNodeWithoutCircularRestrictions<T> Last { get; set; }
        public int Count { get; set; }
        public void AddLast(LinkedListNodeWithoutCircularRestrictions<T> node)
        {
            Last.Next = node;
            Last = node;
            Count++;
        }
        public LinkedListNodeWithoutCircularRestrictions<T> AddLast(T value)
        {
            var node = new LinkedListNodeWithoutCircularRestrictions<T>();
            node.Value = value;
            node.Next = null;
            Last.Next = node;
            Last = node;
            Count++;
            return node;
        }
       
    }
}
