﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._8LoopDetection
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputList = new LinkedListWithoutCircularRestrictions<char>();
            inputList.Last = inputList.First = new LinkedListNodeWithoutCircularRestrictions<char>();
            inputList.First.Value = 'A';
            inputList.Count++;

            inputList.AddLast('B');
            var beginningOfLoopNode = inputList.AddLast('C');
            inputList.AddLast('D');
            var endOfList = inputList.AddLast('E');
            endOfList.Next = beginningOfLoopNode;


            Console.WriteLine("Value of Beginning of Loop with FindBeginningOfLoopUsingLastProperty() is {0}",
                              inputList.FindBeginningOfLoopUsingLastProperty().Value);

            Console.WriteLine("Value of Beginning of Loop with FindBeginningOfLoopUsingHashSet() is {0}",
                              inputList.FindBeginningOfLoopUsingHashSet().Value);


            var nullInputList = new LinkedListWithoutCircularRestrictions<char>();

            Console.WriteLine("Beginning of Loop with FindBeginningOfLoopUsingLastProperty() with null list is {0}",
                              nullInputList.FindBeginningOfLoopUsingLastProperty() == null ? "null" : "not null");

            Console.WriteLine("Beginning of Loop with FindBeginningOfLoopUsingHashSet() with null list is {0}",
                nullInputList.FindBeginningOfLoopUsingHashSet() == null ? "null" : "not null");

            Console.ReadLine();
        }
    }
}
