﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._8LoopDetection
{
    public static class ExtensionMethods
    {
        public static LinkedListNodeWithoutCircularRestrictions<T> FindBeginningOfLoopUsingLastProperty<T>(this LinkedListWithoutCircularRestrictions<T> list)
        {
            if (list.Last != null)
            {
                return list.Last.Next;
            }
            return null;       
        }

        public static LinkedListNodeWithoutCircularRestrictions<T> FindBeginningOfLoopUsingHashSet<T>(this LinkedListWithoutCircularRestrictions<T> list)
        {
            var setOfNodes = new HashSet<LinkedListNodeWithoutCircularRestrictions<T>>();
            var current = list.First;
            while(current != null)
            {
                if (setOfNodes.Contains(current))
                {
                    return current;
                }
                else
                {
                    setOfNodes.Add(current);
                }
                current = current.Next;
            }
            return null;  // list is not a circular list
        }
    }
}
