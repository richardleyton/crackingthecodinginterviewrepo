﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1._2ReverseString
{
    class Program
    {
        static void Main(string[] args)
        {
            string testString;
            testString = "abcdefg";
            reverse(testString);
            Console.ReadLine();
        }
        // 1.2 Implement a function void reverse(char* str) in C or C++ which reverses a nullterminated string.
        public static void reverse(string str)
        {
            StringBuilder newString = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                newString.Append(str.Substring(str.Length - 1 - i, 1));
            }
            Console.WriteLine("Original string is {0}", str);
            Console.WriteLine("Reversed string is {0}", newString);
        }
    }
}
