﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._6Successor
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength = 7;
            //int arrayLength = 6;
            var testArray = new int[arrayLength];
            var root = new TreeNode<int>();
            for (int i = 0; i < 7; i++)
            {
                testArray[i] = i;
            }

            //testArray[0] = 2;
            //testArray[1] = 4;
            //testArray[2] = 6;
            //testArray[3] = 8;
            //testArray[4] = 10;
            //testArray[5] = 20;

            root = root.CreateMinimalTree(testArray);

            //var testNode = root.left;
            //var testNode = root.right;
            //var testNode = root;
            var testNode = root.right.left;
            //var testNode = root.right.right;
            Console.WriteLine("testNode is {0}", testNode.value);

            if (root.FindSuccessor(testNode) == null)
            {
                Console.WriteLine("Successor was null, no successor");
            }
            else
            {
                Console.WriteLine("Successor to {0} is {1}", testNode.value, root.FindSuccessor(testNode).value);
            }

            // Testing withing testNode is root.left, testNode is 1. Successor should be 2.  It is returning 3.
            // Added public static bool found and static TreeNode<T> foundNode and the appropriate logic.  That worked.

            Console.ReadLine();
        }
    }
}
