﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._6Successor
{
    public class TreeNode<T> where T: IComparable<T>
    {
        public T value;
        public TreeNode<T> left;
        public TreeNode<T> right;
        public TreeNode<T> parent;  // Suggested in problem description, but necessary.

        public TreeNode()
        {
        }

        public TreeNode(T v)
        {
            value = v;
        }

        public static TreeNode<T> previous = null;
        public static bool found = false;
        public static TreeNode<T> foundNode = null;
        public TreeNode<T> FindSuccessor(TreeNode<T> givenNode)
        {
            if (this == null)
            {
                return null;
            }
            if (!found && left != null)
            {
                left.FindSuccessor(givenNode);
            }

            // Visit current TreeNode
            Console.WriteLine(value);
            if (!found && previous == givenNode)
            {
                found = true;
                foundNode = this;
                return foundNode;
            }
            previous = this;
            if (!found && right != null)
            {
                right.FindSuccessor(givenNode);
            }

            //return null;
            return foundNode;
        }

        public TreeNode<T> CreateMinimalTree(T[] inputArray)
        {
            int numberOfNodes = inputArray.Length;
            int centerNodeIndex;
            TreeNode<T> centerNode;

            if (numberOfNodes == 1)
            {
                centerNode = new TreeNode<T>(inputArray[0]);
            }
            else if (numberOfNodes == 2)
            {
                centerNode = new TreeNode<T>(inputArray[1]);
                centerNode.left = new TreeNode<T>(inputArray[0]);
            }
            else
            {
                // Handle the center node
                centerNodeIndex = (int)Math.Floor((double)(numberOfNodes / 2));
                centerNode = new TreeNode<T>(inputArray[centerNodeIndex]);

                // Left side of center
                centerNode.left = CreateMinimalTree(inputArray.Take(centerNodeIndex).ToArray<T>());

                // Right side of center
                centerNode.right = CreateMinimalTree(inputArray.Skip(centerNodeIndex + 1).Take(numberOfNodes - centerNodeIndex - 1).ToArray<T>());
            }

            return centerNode;
        }

    }
}
