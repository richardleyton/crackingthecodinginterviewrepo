﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Collections;  // Need for BitArray

namespace IsAllCharsUnique
{
    class Program
    {
        static void Main(string[] args)
        {
            //string testString = "abcdefg";
            // No data structure was fastest

            //string testString = "";
            //string testString = null;

            //string testString = @"`1234567890-=qwertyuiop[]\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:ZXCVBNM<>?";
            // BitArray was fastest

            //string testString = @"`1234567890-=qwertyyuiop[]\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:ZXCVBNM<>?";
            // Repeated y
            // BitArray was fastest

            
            string[] words = { "abcdefg", "", null, 
                @"`1234567890-=qwertyuiop[]\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:ZXCVBNM<>?", 
                @"`1234567890-=qwertyyuiop[]\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:ZXCVBNM<>?" };

            foreach (var word in words)
            {
                Console.WriteLine("Testing IsAllCharsUnique()");

                var stopwatch = Stopwatch.StartNew();
                if (IsAllCharsUnique(word))
                {
                    Console.WriteLine("The string {0} has all unique characters", word);
                }
                else
                {
                    Console.WriteLine("The string {0} does not have all unique characters", word);
                }
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                Console.WriteLine("Testing IsAllCharsUniqueWithNoAdditionalDataStructure()");

                stopwatch = Stopwatch.StartNew();
                if (IsAllCharsUniqueWithNoAdditionalDataStructure(word))
                {
                    Console.WriteLine("The string {0} has all unique characters", word);
                }
                else
                {
                    Console.WriteLine("The string {0} does not have all unique characters", word);
                }
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                Console.WriteLine("Testing IsAllCharsUniqueWithBitArray()");

                stopwatch = Stopwatch.StartNew();
                if (IsAllCharsUniqueWithBitArray(word))
                {
                    Console.WriteLine("The string {0} has all unique characters", word);
                }
                else
                {
                    Console.WriteLine("The string {0} does not have all unique characters", word);
                }
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}\n", stopwatch.Elapsed);
            }
            Console.ReadLine();
        }

        // Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

        public static bool IsAllCharsUnique(string inputString)
        {
            const int MaxUnicodeValue = 65535;  // char: Holds 16-bit Unicode characters. The smallest possible value of a char variable is the Unicode character whose 
            // value is 0;  the largest possible value is the Unicode character whose value is 65,535.
            bool[] charsFound = new bool[MaxUnicodeValue + 1];           // Initialize all entries to false.  Use a builtin method or for loop to initialize all entries to false;
            // Do not need to initialize bool values to false.  That is their default value.
            // http://bytes.com/topic/c-sharp/answers/539457-declare-array-default-value

            // If you want to use 1/8 of the memory to represent the bool array, use BitArray and it's methods:
            // http://www.dotnetperls.com/bitarray
            if (String.IsNullOrEmpty(inputString))
            {
                Console.WriteLine("Empty or null string was input");
                return false;
            }

            for (int i = 0; i < inputString.Length; i++)
            {
                if (charsFound[inputString[i]] == false)
                {
                    charsFound[inputString[i]] = true;
                }
                else
                {
                    return false;
                }
            }
            return true;

        }


        public static bool IsAllCharsUniqueWithNoAdditionalDataStructure(string inputString)
        {
            if (String.IsNullOrEmpty(inputString))
            {
                Console.WriteLine("Empty or null string was input");
                return false;
            }
            for (int i = 0; i < inputString.Length; i++)
            {
                for (int j = i + 1; j < inputString.Length; j++)
                {
                    if (inputString[i] == inputString[j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool IsAllCharsUniqueWithBitArray(string inputString)
        {
            const int MaxUnicodeValue = 65535;  // char: Holds 16-bit Unicode characters. The smallest possible value of a char variable is the Unicode character whose 
            // value is 0;  the largest possible value is the Unicode character whose value is 2^16 -1 = 65,535.

            //If you want to use 1/8 of the memory to represent the bool array, use BitArray and it's methods: http://www.dotnetperls.com/bitarray, 
            // https://msdn.microsoft.com/en-us/library/system.collections.bitarray%28v=vs.110%29.aspx

            BitArray charsFoundBitArray = new BitArray(MaxUnicodeValue + 1);

            if (String.IsNullOrEmpty(inputString))
            {
                Console.WriteLine("Empty or null string was input");
                return false;
            }

            for (int i = 0; i < inputString.Length; i++)
            {
                if (charsFoundBitArray[inputString[i]] == false)
                {
                    charsFoundBitArray[inputString[i]] = true;
                    // Create BitArray from the array.
                    // BitArray bitArray = new BitArray(32);
                    // bitArray[3] = true;     // You can set the bits with the indexer.
                    // bitArray[5] = true;
                    // bitArray.Set(10, true); // You can set the bits with Set.
                    // bool bit = bitArray.Get(i);

                }
                else
                {
                    return false;
                }
            }
            return true;

        }
    }
}
