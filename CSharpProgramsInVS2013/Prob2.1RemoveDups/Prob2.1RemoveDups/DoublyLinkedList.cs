﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._1RemoveDups
{
    public class DoublyLinkedList : LinkedList<int>
    {
        private LinkedList<int> head = null;

        public void RemoveDuplicates()
        {
            //LinkedListNode<int> current = head.First;
            LinkedListNode<int> current;
            LinkedListNode<int> possibleDuplicate;
            LinkedListNode<int> nextPossibleDuplicate;
            if (this != null)
            {
                current = this.First;
                while (current != null && current.Next != null)
                {
                    possibleDuplicate = current.Next;
                    while (possibleDuplicate != null)
                    {
                        if (current.Value == possibleDuplicate.Value)
                        {
                            // possibleDuplicate.Previous = possibleDuplicate.Next;  // Can't do this, read only error
                            nextPossibleDuplicate = possibleDuplicate.Next;
                            this.Remove(possibleDuplicate);
                        }
                        else
                        {
                            nextPossibleDuplicate = possibleDuplicate.Next;
                        }
                        possibleDuplicate = nextPossibleDuplicate;
                    }
                    current = current.Next;
                }
            }
        }

        public void printAllNodes()
        {
            //LinkedListNode<int> current = head.First;
            LinkedListNode<int> current;
            if (this != null)
            {
                current = this.First;
                while (current != null)
                {
                    Console.WriteLine(current.Value);
                    current = current.Next;
                }
            }            
        }
    }
}
