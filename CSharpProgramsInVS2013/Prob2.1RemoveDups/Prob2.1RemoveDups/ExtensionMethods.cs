﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._1RemoveDups
{
    public static class ExtensionMethods
    {
        public static void RemoveDuplicates(this LinkedList<int> head)
        {
            LinkedListNode<int> current;
            LinkedListNode<int> possibleDuplicate;
            LinkedListNode<int> nextPossibleDuplicate;


            if (head != null)
            {
                current = head.First;
                while (current != null && current.Next != null)
                {
                    possibleDuplicate = current.Next;
                    while (possibleDuplicate != null)
                    {
                        if (current.Value == possibleDuplicate.Value)
                        {
                            // possibleDuplicate.Previous = possibleDuplicate.Next;  // Can't do this, read only error
                            nextPossibleDuplicate = possibleDuplicate.Next;
                            head.Remove(possibleDuplicate);
                        }
                        else
                        {
                            nextPossibleDuplicate = possibleDuplicate.Next;
                        }
                        possibleDuplicate = nextPossibleDuplicate;
                    }

                    current = current.Next;
                }
            }
        }
        public static void printAllNodes(this LinkedList<int> head)
        {
            LinkedListNode<int> current = head.First;
            while (current != null)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }
        }
    }
}
