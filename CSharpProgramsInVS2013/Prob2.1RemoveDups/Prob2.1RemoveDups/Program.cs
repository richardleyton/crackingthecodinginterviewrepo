﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._1RemoveDups
{
    class Program
    {
        static void Main(string[] args)
        {
            //SinglyLinkedList testList = new SinglyLinkedList();
            //DoublyLinkedList testList = new DoublyLinkedList();
            //LinkedList<int> testList = new LinkedList<int>();  // Uses ExtensionMethods.cs

            // Could also initialize list with array like this:
            int[] integers = { 1, 2, 2, 4, 2, 1, 7 };
            LinkedList<int> testList = new LinkedList<int>(integers);

            //testList.AddFirst(7);
            //testList.AddFirst(6);
            //testList.AddFirst(5);
            //testList.AddFirst(4);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(1);

            //testList.AddFirst(7);
            //testList.AddFirst(6);
            //testList.AddFirst(5);
            //testList.AddFirst(4);
            //testList.AddFirst(3);
            //testList.AddFirst(1);
            //testList.AddFirst(1);

            //testList.AddFirst(7);
            //testList.AddFirst(6);
            //testList.AddFirst(5);
            //testList.AddFirst(4);
            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(1);

            //testList.AddFirst(7);
            //testList.AddFirst(6);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(1);

            //testList.AddFirst(7);
            //testList.AddFirst(2);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(1);

            //testList.AddFirst(7);
            //testList.AddFirst(2);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(2);

            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(2);

            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(2);

            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(5);
            //testList.AddFirst(2);
            //testList.AddFirst(3);
            //testList.AddFirst(2);
            //testList.AddFirst(2);

            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(2);
            //testList.AddFirst(2);


            Console.WriteLine("testList");
            testList.printAllNodes();

            testList.RemoveDuplicates();
            Console.WriteLine("RemoveDuplicates");
            testList.printAllNodes();
            Console.ReadLine();
        }
    }
}
