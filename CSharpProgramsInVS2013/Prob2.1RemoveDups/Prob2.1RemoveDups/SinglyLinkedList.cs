﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._1RemoveDups
{
    // 2.1 Remove Dups: Write code to remove duplicates from an unsorted linked list.
    // FOLLOW UP
    // How would you solve this problem if a temporary buffer is not allowed?
    // Started 7-20

    // http://stackoverflow.com/questions/3823848/creating-a-very-simple-linked-list

    // http://stackoverflow.com/questions/20087194/c-sharp-singly-linked-list-implementation

    // Questions for interviewer:
    // 1) Singly or doubly linked list?
    // 2) Does the order matter?  Do we need to keep the same order?
    //


    public class SinglyLinkedList
    {
        public class Node
        {
            public Node next = null;
            public int data;
        }

        private Node head = null;

        public void RemoveDuplicates()
        {
            // Brute force. Check each entry for duplicates amongst the remaining nodes in the list.
            // Big O for N being the number of nodes in the list is N - 1 + N - 2 + ... + 1 = 
            // N * N / 2.  Big O(N^2)

            Node current = head;
            Node previous = head;
            Node possibleDuplicate;
            while (current != null && current.next != null)
            {
                possibleDuplicate = current.next;
                while (possibleDuplicate != null)
                {
                    if (current.data == possibleDuplicate.data)
                    {
                        previous.next = possibleDuplicate.next;
                    }
                    else
                    {
                        previous = previous.next;
                    }
                    possibleDuplicate = possibleDuplicate.next;
                }

                current = current.next;
                previous = current;
            } 
        }

        public void AddFirst(int data)
        {
            Node toAdd = new Node();

            toAdd.data = data;
            toAdd.next = head;

            head = toAdd;
        }
        public void printAllNodes()
        {
            Node current = head;
            while (current != null)
            {
                Console.WriteLine(current.data);
                current = current.next;
            }
        }
    }
}
