﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._4Partition
{
    public class Program
    {
        public static void Main(string[] args)
        {
            float partition = 5;

            float[] vals = { 3, 5, 8, 5, 10, 2, 1 };
            LinkedList<float> testList = new LinkedList<float>(vals);

            LinkedList<float> resultList;

            Console.WriteLine("partition = {0}", partition);
            Console.WriteLine("Input:");
            foreach (float val in testList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            resultList = Partition(testList, partition);
            Console.WriteLine("Output:");
            foreach (float val in resultList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            partition = 5.7F;
            Console.WriteLine("partition = {0}", partition);
            Console.WriteLine("Input:");
            foreach (float val in testList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            resultList = Partition(testList, partition);
            Console.WriteLine("Output:");
            foreach (float val in resultList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            Console.ReadLine();
        }

        public static LinkedList<float> Partition(LinkedList<float> myList, float pivotValue)
        {
            var newList = new LinkedList<float>();

            LinkedListNode<float> currentNodeOriginalList = myList.First;
            LinkedListNode<float> originNodeNewList = newList.AddFirst(pivotValue);
            while (currentNodeOriginalList != null)
            {              
                if (currentNodeOriginalList.Value < pivotValue)
                {
                    newList.AddFirst(currentNodeOriginalList.Value);
                }
                else if (currentNodeOriginalList.Value == pivotValue)
                {                   
                    newList.AddAfter(originNodeNewList, pivotValue);                 
                }
                else
                {
                    newList.AddLast(currentNodeOriginalList.Value);
                }
                currentNodeOriginalList = currentNodeOriginalList.Next;
            }
            // Delete Origin node
            newList.Remove(originNodeNewList);
            return newList;
        }
    }
}
