﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._6Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] testVals = { 1, 2, 3, 3, 2, 1 };
            //int[] testVals = { 1, 2, 3, 2, 1 };

            //int[] testVals = { 1, 2, 3, 1, 2, 3 };
            //int[] testVals = { 1, 2, 3, 1, 2, 1 };
            //int[] testVals = { 1, 2, 3, 3, 2, 0 };
            var testList = new LinkedList<int>(testVals);
            //LinkedList<int> testList = null;

            //var testList = new LinkedList<int>();

            Console.Write("Input List: ");
            if (testList != null)
            {
                foreach (int val in testList)
                {
                    Console.Write("{0} ", val);
                }
            }
            else
            {
                Console.WriteLine("testList is null");
            }
            Console.WriteLine();
            Console.WriteLine("The list is {0}a palindrome", testList.IsListPalindrome() ? "" : "not ");

            Console.ReadLine();
        }
    }
}
