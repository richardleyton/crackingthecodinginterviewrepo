﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._6Palindrome
{
    public static class ExtensionMethods
    {
        public static bool IsListPalindrome(this LinkedList<int> inputList)
        {
            if (inputList == null || inputList.First == null)
            {
                return false;
            }
            LinkedListNode<int> frontNode = inputList.First;
            LinkedListNode<int> backNode = inputList.Last;

            for (int i = 0; i < inputList.Count / 2; i++)
            {
                if (frontNode.Value != backNode.Value)
                {
                    return false;
                }
                frontNode = frontNode.Next;
                backNode = backNode.Previous;
            }
            return true;
        }
    }
}
