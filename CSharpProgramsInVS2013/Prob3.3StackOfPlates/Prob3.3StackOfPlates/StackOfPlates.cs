﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._3StackOfPlates
{
    public class SetOfStacks<T> : Stack<T>
    {
        //var stackOfStacks = new Stack<Stack<T>>();
        Stack<Stack<T>> stackOfStacks = new Stack<Stack<T>>();

        //int defaultCapacity = 10;
        static int defaultCapacity = 10;
        int individualStackCapacity = defaultCapacity;
        Stack<T> currentStack;
        int currentStackIndex = 0;
        int countTotalOfAllStacks = 0;

        public SetOfStacks(int stackCapacity)
        {
            individualStackCapacity = stackCapacity;
        }

        public SetOfStacks() {}

        public T PopStackOfStacks()
        {
            T poppedValueToReturn;

            if (countTotalOfAllStacks == 0)
            {
                //throw Exception("No nodes left in stacks"); 
                throw new Exception("No nodes left in stacks");
            }
            else if (currentStack.Count == 0)
            {
                while (currentStack.Count == 0)
                {
                    Pop();
                    //currentStack = Peek();
                    currentStack = this.stackOfStacks.Peek();
                }
            }
        
            poppedValueToReturn = currentStack.Pop();
            countTotalOfAllStacks--;
            return poppedValueToReturn;
        }

        public void PushStackOfStacks(T value)
        {
            if (stackOfStacks.Count == 0)
            {
                currentStack = new Stack<T>(individualStackCapacity);
                this.stackOfStacks.Push(currentStack);           
            }
            else
            {
                currentStack = this.stackOfStacks.Peek();
            }

            if (currentStack.Count > individualStackCapacity)
            {
                var newStack = new Stack<T>(individualStackCapacity);
                this.stackOfStacks.Push(newStack);
                //currentStack = Peek();
                currentStack = this.stackOfStacks.Peek();            
            }
            currentStack.Push(value);
            countTotalOfAllStacks++;
                
        }
    
        public T PopAt(int index)  // index starts at 1 for first stack
        {
            if (index < 0 || stackOfStacks.Count < index)
            {
                //Need to change throw Exception("SubStack #{0} doesn't exist.", index); to 
                throw new Exception(String.Format("SubStack #{0} doesn't exist.", index));
            }
            T poppedValueToReturn = this.stackOfStacks.ElementAt(index - 1).Pop();
            countTotalOfAllStacks--;
            return poppedValueToReturn;
        }
    }
}
