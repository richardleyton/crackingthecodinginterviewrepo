﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._3StackOfPlates
{
    class Program
    {
        static void Main(string[] args)
        {
            var testSetOfStacks = new SetOfStacks<int>(5);
            testSetOfStacks.PushStackOfStacks(1);
            testSetOfStacks.PushStackOfStacks(2);
            testSetOfStacks.PushStackOfStacks(3);
            testSetOfStacks.PushStackOfStacks(4);
            testSetOfStacks.PushStackOfStacks(5);
            testSetOfStacks.PushStackOfStacks(6);
            testSetOfStacks.PushStackOfStacks(7);
            testSetOfStacks.PushStackOfStacks(8);
            testSetOfStacks.PushStackOfStacks(9);
            testSetOfStacks.PushStackOfStacks(10);
            testSetOfStacks.PushStackOfStacks(11);
            testSetOfStacks.PushStackOfStacks(12);
            testSetOfStacks.PushStackOfStacks(13);
            testSetOfStacks.PushStackOfStacks(14);
            testSetOfStacks.PushStackOfStacks(15);
            testSetOfStacks.PushStackOfStacks(16);
            testSetOfStacks.PushStackOfStacks(17);
            testSetOfStacks.PushStackOfStacks(18);
            testSetOfStacks.PushStackOfStacks(19);
            testSetOfStacks.PushStackOfStacks(20);
            testSetOfStacks.PushStackOfStacks(21);
            testSetOfStacks.PushStackOfStacks(22);
            testSetOfStacks.PushStackOfStacks(23);

            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());
            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());
            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());
            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());
            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());

            testSetOfStacks.PushStackOfStacks(24);

            Console.WriteLine("PopAt on substack 2: {0}", testSetOfStacks.PopAt(2));
            //Console.WriteLine("PopAt on substack 5: {0}", testSetOfStacks.PopAt(5));  // This throws an unhandled exception of 
                                                                                        // type 'System.Exception' as expected.
            Console.WriteLine("PopAt on substack 3: {0}", testSetOfStacks.PopAt(3));
            Console.WriteLine("PopAt on substack 4: {0}", testSetOfStacks.PopAt(4));

            Console.WriteLine("PopStackOfStacks: {0}", testSetOfStacks.PopStackOfStacks());
            //Console.WriteLine("PopAt: {0}", testSetOfStacks.PopAt(10));  // This throws an unhandled exception of 
                                                                           // type 'System.Exception' as expected.

            Console.ReadLine();

        }
    }
}
