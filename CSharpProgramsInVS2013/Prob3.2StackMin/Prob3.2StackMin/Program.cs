﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._2StackMin
{
    class Program
    {
        static void Main(String[] args)
        {
            var testStack = new MyStack<int>();
            testStack.Push(5);
            Console.WriteLine("Pushed 5.  min is {0}", testStack.GetMin());
            testStack.Push(4);
            Console.WriteLine("Pushed 4.  min is {0}", testStack.GetMin());
            testStack.Push(3);
            Console.WriteLine("Pushed 3.  min is {0}", testStack.GetMin());
            testStack.Push(-1);
            Console.WriteLine("Pushed -1.  min is {0}", testStack.GetMin());
            testStack.Push(2);
            Console.WriteLine("Pushed 2.  min is {0}", testStack.GetMin());
            testStack.Push(1);
            Console.WriteLine("Pushed 1.  min is {0}", testStack.GetMin());

            Console.WriteLine("Stack contains, starting with the top:");
            StackNode<int> current = testStack.Top;
            while (current != null)
            {
                Console.Write("{0} ", current.data);
                current = current.next;
            }
            Console.WriteLine();

            while (testStack.Top != null)
            {
                Console.Write("Popped {0}.  ", testStack.Pop());
                Console.WriteLine("min is {0}", testStack.GetMin());
            }
    
            Console.ReadLine();
        }
    }
}
