﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._2StackMin
{
    public class StackNode<T> where T : IComparable<T>
    {
        public T data;
        public StackNode<T> next;

        public StackNode(T data)
        {
            this.data = data;
        }
    }
    public class MyStack<T> where T : IComparable<T>
    {
        private StackNode<T> top;

        public StackNode<T> Top
        {
            get { return top; }  // Using get; only to keep somewhat private like example on P. 96
            //set { top = value; }
        }
        
        //private T min = T.MaxValue;
        static FieldInfo maxValueField = typeof(T).GetField("MaxValue", BindingFlags.Public | BindingFlags.Static);
        //if (maxValueField == null)
        //    throw new NotSupportedException(typeof(T).Name);
        static T maxValue = (T)maxValueField.GetValue(null);
        private T min = maxValue;
        public T Pop()
        {
            if (top == null)
            {
                throw new Exception("Trying to pop an empty stack.");
            }
            T item = top.data;
            top = top.next;
            //if (item == min)
            //if (item.Equals(min))
            if (item.CompareTo(min) == 0)

            {
                min = FindNewMin();
            }
            return item;
        }

        public void Push(T item)
        {
            StackNode<T> t = new StackNode<T>(item);
            t.next = top;
            top = t;
            //if (item < min)
            if (item.CompareTo(min) < 0)
            {
                min = item;
            }
        }

        public T GetMin()
        {
            return min;
        }

        private T FindNewMin()
        {
            StackNode<T> current = top;
            min = maxValue;
            while (current != null)
            {
                //if (current.data < min)
                if (current.data.CompareTo(min) < 0)
                {
                    min = current.data;
                }
                current = current.next;
            }
            return min;
        }
    }
}
