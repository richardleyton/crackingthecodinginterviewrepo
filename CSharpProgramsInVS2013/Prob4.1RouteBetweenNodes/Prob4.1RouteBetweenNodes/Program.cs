﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._1RouteBetweenNodes
{
    class Program
    {
        static void Main(string[] args)
        {
            var testGraph = new Graph();

            // Build P. 106 Graph Search Example for test case.
            var node0 = new Node("node0(Root)");
            var node1 = new Node("node1");
            var node2 = new Node("node2");
            var node3 = new Node("node3");
            var node4 = new Node("node4");
            var node5 = new Node("node5");

            node0.children = new Node[] { node1, node4, node5 };
            node1.children = new Node[] { node3, node4 };
            node2.children = new Node[] { node1 };
            node3.children = new Node[] { node2, node4 };
            
            // node4 and node5 have no children

            //testGraph.FindRoute(node1, node2);  
            //testGraph.FindRoute(node0, node2);
            //testGraph.FindRoute(node5, node0);
            //testGraph.FindRoute(node3, node3);
            testGraph.FindRoute(node3, node5);
            Console.ReadLine();
        }
    }
}
