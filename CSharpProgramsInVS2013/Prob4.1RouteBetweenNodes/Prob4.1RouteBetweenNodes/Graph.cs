﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._1RouteBetweenNodes
{
    public class Graph
    {
        public Node[] nodes;  // This was not used.

        public void FindRoute(Node root, Node destination)
        {
            Queue<Node> queue = new Queue<Node>();
            root.marked = true;
            queue.Enqueue(root); // Add to end of queue

            while (queue.Count != 0)
            {
                Node r = queue.Dequeue(); // Remove from front of queue
                if (Visit(r, root, destination))
                {
                    return;
                }
                if (r.children != null)
                {
                    foreach (Node n in r.children)
                    {
                        if (n.marked == false)
                        {
                            n.marked = true;
                            queue.Enqueue(n);
                        }
                    }
                }
            }
            Console.WriteLine("Destination was not found.  There is no route between {0} and {1}", root.name, destination.name);
        }

        public bool Visit(Node visitedNode, Node rootNode, Node destinationNode)
        {
            Console.WriteLine("visitedNode is {0}", visitedNode.name);
            if (visitedNode == destinationNode)
            {
                Console.WriteLine("Destination was found.  There is a route between {0} and {1}", rootNode.name, destinationNode.name);
                return true;
            }
            return false;
        }
    }
}
