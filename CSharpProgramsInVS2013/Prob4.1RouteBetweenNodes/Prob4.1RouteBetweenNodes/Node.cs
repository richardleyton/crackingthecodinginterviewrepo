﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._1RouteBetweenNodes
{
    public class Node
    {
        public string name;
        public Node[] children;  // Also called adjacent nodes
        public bool marked;      // Also called visited

        public Node()
        {
        }

        public Node(string n)
        {
            name = n;
        }
    }
}
