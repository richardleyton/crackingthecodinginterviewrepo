﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._5SumLists
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] reverseArray1 = {7, 1, 6};
            //int[] reverseArray2 = {5, 9, 2};
            //int[] forwardArray1 = {6, 1, 7};
            //int[] forwardArray2 = {2, 9, 5};

            int[] reverseArray1 = { 9, 7, 8 };
            int[] reverseArray2 = { 6, 8, 5 };
            int[] forwardArray1 = { 8, 7, 9 };
            int[] forwardArray2 = { 5, 8, 6 };

            var reverseList1 = new LinkedList<int>(reverseArray1);
            var reverseList2 = new LinkedList<int>(reverseArray2);
            var forwardList1 = new LinkedList<int>(forwardArray1);
            var forwardList2 = new LinkedList<int>(forwardArray2);

            Console.WriteLine("reverseList1: ");
            foreach (int val in reverseList1)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            Console.WriteLine("reverseList2: ");
            foreach (int val in reverseList2)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine(); 

            Console.WriteLine("forwardList1: ");
            foreach (int val in forwardList1)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine(); 

            Console.WriteLine("forwardList2: ");
            foreach (int val in forwardList2)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            var reverseSumList = LinkedListSumReversedOrder(reverseList1, reverseList2);

            Console.WriteLine("reverseSumList for LinkedListSumReversedOrder: ");
            foreach (int val in reverseSumList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            reverseSumList = LinkedListSumReversedOrderConvertToInt(reverseList1, reverseList2);

            Console.WriteLine("reverseSumList for LinkedListSumReversedOrderConvertToInt: ");
            foreach (int val in reverseSumList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            var forwardSumList = LinkedListSumForwardOrder(forwardList1, forwardList2);
            Console.WriteLine("forwardSumList for LinkedListSumForwardOrder: ");
            foreach (int val in forwardSumList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            forwardSumList = LinkedListSumForwardOrderConvertToInt(forwardList1, forwardList2);
            Console.WriteLine("forwardSumList for LinkedListSumForwardOrderConvertToInt: ");
            foreach (int val in forwardSumList)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine();

            Console.ReadLine();
        }

        public static LinkedList<int> LinkedListSumReversedOrder(LinkedList<int> list1, LinkedList<int> list2)
        {
            if (list1 == null || list2 == null || list1.First == null || list2.First == null)
            {
                Console.WriteLine("Lists must not be null or empty");
                return null;
            }

            LinkedListNode<int> currentNodeList1 = list1.First;
            LinkedListNode<int> currentNodeList2 = list2.First;
            var sumList = new LinkedList<int>();
            int carriedOne = 0;

            while(currentNodeList1 != null || currentNodeList2 != null)
            {
                if (currentNodeList1 == null)
                {
            
                    if (currentNodeList2.Value + carriedOne > 9)
                    {
                        sumList.AddLast((currentNodeList2.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddLast(currentNodeList2.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList2 = currentNodeList2.Next;
                }
                else if (currentNodeList2 == null)
                {
                    if (currentNodeList1.Value + carriedOne > 9)
                    {
                        sumList.AddLast((currentNodeList1.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddLast(currentNodeList1.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList1 = currentNodeList1.Next;
                }
                else
                {
                    if (currentNodeList1.Value + currentNodeList2.Value + carriedOne > 9)
                    {
                        sumList.AddLast((currentNodeList1.Value + currentNodeList2.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddLast(currentNodeList1.Value + currentNodeList2.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList1 = currentNodeList1.Next;
                    currentNodeList2 = currentNodeList2.Next;       
                }  
            }
            if (carriedOne == 1)
            {
                sumList.AddLast(1);
            }
            return sumList;
        }

        public static LinkedList<int> LinkedListSumForwardOrder(LinkedList<int> list1, LinkedList<int> list2)
        {
            if (list1 == null || list2 == null || list1.First == null || list2.First == null)
            {
                Console.WriteLine("Lists must not be null or empty");
                return null;
            }

            LinkedListNode<int> currentNodeList1 = list1.Last;
            LinkedListNode<int> currentNodeList2 = list2.Last;
            var sumList = new LinkedList<int>();
            int carriedOne = 0;

            while(currentNodeList1 != null || currentNodeList2 != null)
            {
                if (currentNodeList1 == null)
                {
            
                    if (currentNodeList2.Value + carriedOne > 9)
                    {
                        sumList.AddFirst((currentNodeList2.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddFirst(currentNodeList2.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList2 = currentNodeList2.Previous;
                }
                else if (currentNodeList2 == null)
                {
                    if (currentNodeList1.Value + carriedOne > 9)
                    {
                        sumList.AddFirst((currentNodeList1.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddFirst(currentNodeList1.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList1 = currentNodeList1.Previous;
                }
                else
                {
                    if (currentNodeList1.Value + currentNodeList2.Value + carriedOne > 9)
                    {
                        sumList.AddFirst((currentNodeList1.Value + currentNodeList2.Value + carriedOne) % 10);
                        carriedOne = 1;
                    }
                    else
                    {
                        sumList.AddFirst(currentNodeList1.Value + currentNodeList2.Value + carriedOne);
                        carriedOne = 0;
                    }
                    currentNodeList1 = currentNodeList1.Previous;
                    currentNodeList2 = currentNodeList2.Previous;       
                }  
            }
            if (carriedOne == 1)
            {
                sumList.AddFirst(1);
            }
            return sumList;
        }



        public static LinkedList<int> LinkedListSumReversedOrderConvertToInt(LinkedList<int> list1, LinkedList<int> list2)
        {
            if (list1 == null || list2 == null || list1.First == null || list2.First == null)
            {
                Console.WriteLine("Lists must not be null or empty");
                return null;
            }

            var sumList = new LinkedList<int>();
            var list1IntString = new StringBuilder();
            var list2IntString = new StringBuilder();
            int list1Int;
            int list2Int;
            int sumListInt;
            string sumListIntString;
            LinkedListNode<int> current = list1.Last;


            while (current != null)
            {
                list1IntString.Append (current.Value);
                current = current.Previous;
            }

            current = list2.Last;
            while (current != null)
            {
                list2IntString.Append(current.Value);
                current = current.Previous;        
            }

            if (!Int32.TryParse(list1IntString.ToString(), out list1Int))
            {
                Console.WriteLine("Values in nodes of list1 must only contain decimal digits");
                return null;
            }

            if (!Int32.TryParse(list2IntString.ToString(), out list2Int))
            {
                Console.WriteLine("Values in nodes of list2 must only contain decimal digits");
                return null;
            }

            sumListInt = list1Int + list2Int;
            sumListIntString = sumListInt.ToString();
            for (int i = 0; i < sumListIntString.Length; i++)
            {
                sumList.AddFirst(sumListIntString[i] - '0');
            }
            return sumList;
        }


        public static LinkedList<int> LinkedListSumForwardOrderConvertToInt(LinkedList<int> list1, LinkedList<int> list2)
        {
            if (list1 == null || list2 == null || list1.First == null || list2.First == null)
            {
                Console.WriteLine("Lists must not be null or empty");
                return null;
            }

            var sumList = new LinkedList<int>();
            var list1IntString = new StringBuilder();
            var list2IntString = new StringBuilder();
            int list1Int;
            int list2Int;
            int sumListInt;
            string sumListIntString;
            LinkedListNode<int> current = list1.First;


            while (current != null)
            {
                list1IntString.Append(current.Value);
                current = current.Next;
            }

            current = list2.First;
            while (current != null)
            {
                list2IntString.Append(current.Value);
                current = current.Next;        
            }

            if (!Int32.TryParse(list1IntString.ToString(), out list1Int))
            {
                Console.WriteLine("Values in nodes of list1 must only contain decimal digits");
                return null;
            }

            if (!Int32.TryParse(list2IntString.ToString(), out list2Int))
            {
                Console.WriteLine("Values in nodes of list2 must only contain decimal digits");
                return null;
            }

            sumListInt = list1Int + list2Int;
            sumListIntString = sumListInt.ToString();
            for (int i = 0; i < sumListIntString.Length; i++)
            {
                sumList.AddLast(sumListIntString[i] - '0');
            }
            return sumList;
        }
    }
}
