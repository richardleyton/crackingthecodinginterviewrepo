﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._5OneAwayOrLess6thEdition
{
    public static class ExtensionMethods
    {
        // 1.5OneAwayOrLess6thEdition One away or less. Insert, remove or replace one character.
        // O(N).  Started 7-14 @ 8:30 PM.

        public static bool IsOneAwayOrLess(this string inputString, string oneAwayString)
        {
            int lengthInputString = inputString.Length;
            int lengthOneAwayString = oneAwayString.Length;
            int numberOfDifferences = 0;
            if (lengthInputString == lengthOneAwayString)
            {
                // Could be replace or no change
                for (int i = 0; i < lengthInputString; i++)
                {
                    if (inputString[i] != oneAwayString[i])
                    {
                        numberOfDifferences++;
                        if (numberOfDifferences > 1)
                        {
                            return false;
                        }
                    }
                }
            }
            else if (lengthOneAwayString == lengthInputString + 1)
            {
                // Could be insert
                for (int i = 0; i < lengthInputString; i++)
                {
                    if (inputString[i] != oneAwayString[i + numberOfDifferences])
                    {
                        numberOfDifferences++;
                        if (numberOfDifferences > 1)
                        {
                            return false;
                        }
                        else if (inputString[i] != oneAwayString[i + numberOfDifferences])
                        {
                            numberOfDifferences++;
                            return false;
                        }
                    }
                }
            }
            else if (lengthOneAwayString == lengthInputString - 1)
            {
                // Could be remove
                // Make sure that we do not attempt to access beyond the end of the oneAwayString. 
                // inputString =   "abcd", oneAwayString = "abc"  => return true
                // inputString =   "abfd", oneAwayString = "abd"  => return true
                // inputString =   "abff", oneAwayString = "abd"  => return false
                for (int i = 0; i < lengthInputString - 1; i++)
                {
                    if (inputString[i + numberOfDifferences] != oneAwayString[i])
                    {
                        numberOfDifferences++;
                        if (numberOfDifferences > 1)
                        {
                            return false;
                        }
                        else if (inputString[i + numberOfDifferences] != oneAwayString[i])
                        {
                            numberOfDifferences++;
                            return false;
                        }
                    }
                }
            }
            else
            {
                // Not one away
                return false;
            }
            return true;
        }
    }
}
