﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._5OneAwayOrLess6thEdition
{
    class Program
    {
        static void Main(string[] args)
        {
            //string original = "pale";
            //string oneAway = "ple";

            //string original = "pales";
            //string oneAway = "pale";

            //string original = "pale";
            //string oneAway = "bale";

            //string original = "pale";
            //string oneAway = "bake";

            //string original = "pale";
            //string oneAway = "pale";

            //string original = "paless";
            //string oneAway = "pale";

            //string original = "abcd";
            //string oneAway = "abc";

            //string original = "abfd";
            //string oneAway = "abd";

            string original = "abff";
            string oneAway = "abd";
            Console.WriteLine("{0} is{1} one away or less from {2}", oneAway, original.IsOneAwayOrLess(oneAway) ? "": " not"
                                , original);
            Console.ReadLine();
        }
    }
}
