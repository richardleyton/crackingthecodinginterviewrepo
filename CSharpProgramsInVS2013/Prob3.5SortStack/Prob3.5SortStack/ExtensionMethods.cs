﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._5SortStack
{
    public static class ExtensionMethods
    {
        public static Stack<T> SortStack<T>(this Stack<T> inputStack)  where T : IComparable<T>
        {
            Stack<T> tempStack = new Stack<T>();
            T currentMax, nextValue;
            bool itemSwitched;
            int itemSwitchedCountForMetrics= 0;
            int passesMadeThroughStackEachWayForMetrics = 0;

            if (inputStack == null || inputStack.Count == 0)
            {
                throw new Exception("Cannot call SortStack with a null or empty stack");
            }

            if (inputStack.Count == 1)
            {
                return inputStack;
            }

            //currentMax = inputStack.Pop();

            do
            {
                currentMax = inputStack.Pop();
                itemSwitched = false;
                passesMadeThroughStackEachWayForMetrics++;
                while (inputStack.Count != 0)
                {
                    nextValue = inputStack.Pop();

                    //if (currentMax > nextValue)
                    if (currentMax.CompareTo(nextValue) > 0)
                    {
                        tempStack.Push(nextValue);
                        itemSwitched = true;
                        itemSwitchedCountForMetrics++;
                    }
                    else
                    {
                        tempStack.Push(currentMax);
                        currentMax = nextValue;
                    }
                }
                tempStack.Push(currentMax);

                passesMadeThroughStackEachWayForMetrics++;
                while (tempStack.Count != 0)
                {
                    inputStack.Push(tempStack.Pop());
                }
            }
            while (itemSwitched);

            Console.WriteLine("itemSwitchedCountForMetrics {0}", itemSwitchedCountForMetrics);
            Console.WriteLine("passesMadeThroughStackEachWayForMetrics {0}", passesMadeThroughStackEachWayForMetrics);

            return inputStack;
        }

        public static Stack<T> SortStackVersion2<T>(this Stack<T> inputStack) where T : IComparable<T>
        {
            Stack<T> tempStack = new Stack<T>();
            T   currentMax, 
                currentMin,
                nextValue;
            bool itemSwitched;
            int itemSwitchedCountForMetrics = 0;
            int passesMadeThroughStackEachWayForMetrics = 0;

            if (inputStack == null || inputStack.Count == 0)
            {
                throw new Exception("Cannot call SortStack with a null or empty stack");
            }

            if (inputStack.Count == 1)
            {
                return inputStack;
            }

            //currentMax = inputStack.Pop();

            do
            {
                currentMax = inputStack.Pop();
                itemSwitched = false;
                passesMadeThroughStackEachWayForMetrics++;
                while (inputStack.Count != 0)
                {
                    nextValue = inputStack.Pop();

                    //if (currentMax > nextValue)
                    if (currentMax.CompareTo(nextValue) > 0)
                    {
                        tempStack.Push(nextValue);
                        itemSwitched = true;
                        itemSwitchedCountForMetrics++;
                    }
                    else
                    {
                        tempStack.Push(currentMax);
                        currentMax = nextValue;
                    }
                }
                tempStack.Push(currentMax);

                currentMin = tempStack.Pop();
                itemSwitched = false;
                passesMadeThroughStackEachWayForMetrics++;
                while (tempStack.Count != 0)
                {
                    //inputStack.Push(tempStack.Pop());

                    nextValue = tempStack.Pop();

                    if (currentMin.CompareTo(nextValue) < 0)
                    {
                        inputStack.Push(nextValue);
                        itemSwitched = true;
                        itemSwitchedCountForMetrics++;
                    }
                    else
                    {
                        inputStack.Push(currentMin);
                        currentMin = nextValue;
                    }
                }
                inputStack.Push(currentMin);
            }
            while (itemSwitched);

            Console.WriteLine("itemSwitchedCountForMetrics {0}", itemSwitchedCountForMetrics);
            Console.WriteLine("passesMadeThroughStackEachWayForMetrics {0}", passesMadeThroughStackEachWayForMetrics);

            return inputStack;
        }

    }
}
