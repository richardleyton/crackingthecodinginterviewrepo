﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._5SortStack
{
    class Program
    {
        static void Main(string[] args)
        {
            var testStack = new Stack<int>();
            //testStack.Push(5);
            //testStack.Push(4);
            //testStack.Push(3);
            //testStack.Push(2);
            //testStack.Push(1);

            //testStack.Push(1);

            //test without a Push

            //testStack.Push(1);
            //testStack.Pop();

            //testStack.Push(1);
            //testStack.Push(2);
            //testStack.Push(3);
            //testStack.Push(4);
            //testStack.Push(5);

            for (int i = 1; i < 1000; i++)
                testStack.Push(i);
           

            //testStack.SortStack();
            testStack.SortStackVersion2();
            Console.WriteLine("Top of stack {0}", testStack.Peek());
            Console.ReadLine();
        }
    }
}
