﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._4QueueViaStacks
{
    class Program
    {
        static void Main(string[] args)
        {
            var testQueue = new MyQueue<int>();
            testQueue.Enqueue(1);
            testQueue.Enqueue(2);
            testQueue.Enqueue(3);
            testQueue.Enqueue(4);
            testQueue.Enqueue(5);
            testQueue.Enqueue(6);
            testQueue.Enqueue(7);
            Console.WriteLine("DequeueOrPeek('p'): {0}", testQueue.DequeueOrPeek('p'));
            Console.WriteLine("DequeueOrPeek('p'): {0}", testQueue.DequeueOrPeek('p'));
            Console.WriteLine("DequeueOrPeek('d'): {0}", testQueue.DequeueOrPeek('d'));
            Console.WriteLine("DequeueOrPeek('p'): {0}", testQueue.DequeueOrPeek('p'));
            testQueue.Enqueue(8);
            testQueue.Enqueue(9);
            Console.WriteLine("DequeueOrPeek('d'): {0}", testQueue.DequeueOrPeek('d'));
            Console.ReadLine();
        }
    }
}
