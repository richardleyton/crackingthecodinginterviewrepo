﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob3._4QueueViaStacks
{
    public class MyQueue<T> : Stack<T>
    {

        Stack<T> primaryStack = new Stack<T>();
        Stack<T> secondaryStack = new Stack<T>();

        public void Enqueue(T value)
        {
            if (primaryStack.Count == 0 && secondaryStack.Count == 0)
            {
                primaryStack.Push(value);
            }
            else if (primaryStack.Count != 0)
            {
                primaryStack.Push(value);
            }
            else
            {
                while (secondaryStack.Count != 0)
                {
                    primaryStack.Push(secondaryStack.Pop());
                }
                primaryStack.Push(value);
            }
        }

        public T DequeueOrPeek(char dOrP)
        {
            if (primaryStack.Count == 0 && secondaryStack.Count == 0)
            {
                throw new Exception("Can't Dequeue or Peek from an empty queue.");
            }

            if (secondaryStack.Count == 0)
            {
                while (primaryStack.Count != 0)
                {
                    secondaryStack.Push(primaryStack.Pop());
                }
            }

            if (Char.ToLower(dOrP) == 'd')
            {
                return secondaryStack.Pop();
            }
            if (Char.ToLower(dOrP) == 'p')
            {
                return secondaryStack.Peek();
            }
            throw new Exception("DequeueOrPeek must be called with 'd' for Dequeue or 'p' for Peek");
        }
    }
}
