﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._4PalindromePermutation6th
{
    public static class ExtensionMethods
    {
        // 6th Edition Palindrome Permutation.  Started 5:50 pm.  Finished Notepad version at 7 PM.
        // Big O is O(N) where N is length of the string
        public static bool IsPalindromePermutation(this string inputString)
        {
            int[] charCountArray = new int[26];
            int oddNumberOfChars = 0;
            // only look at alphabetic characters in inputString.  Convert all chars to uppercase
            for (int i = 0; i < inputString.Length; i++)
            {
                if (char.IsLetter(inputString[i]))
                {
                    //int index = char.ToUpper(c) - 'A';
                    int index = char.ToUpper(inputString[i]) - 'A';                   
                    charCountArray[index] ++;
                }
            }

            for (int i = 0; i < 26; i++)
            {
                if (charCountArray[i] % 2 != 0)
                {
                    oddNumberOfChars += 1;
                }
            }
            if (oddNumberOfChars > 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
