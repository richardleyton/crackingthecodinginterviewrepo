﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._4PalindromePermutation6th
{
    class Program
    {
        static void Main(string[] args)
        {
            //string testString = "Tact Coa";
            //string testString = "Tact Coab";
            //string testString = "aabbccddeef";
            //string testString = "aabbccddeefgg";
            string testString = "aabbccddeefggg";
            Console.WriteLine("{0} is{1}a palindrome permutation", testString, 
                                testString.IsPalindromePermutation() ? " " : " not ");
            Console.ReadLine();
        }
    }
}
