﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._8ZeroMatrix6thEdition
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[,] testArray = {
            //                       {1, 2, 3},
            //                       {4, 5, 6},
            //                       {7, 8, 9},
            //                       {10, 11, 12}
            //                   };

            //int[,] testArray = {
            //                       {1, 0, 3},
            //                       {4, 5, 6},
            //                       {7, 8, 9},
            //                       {10, 11, 12}
            //                   };

            //int[,] testArray = {
            //                       {1, 2, 3},
            //                       {4, 5, 0},
            //                       {7, 8, 9},
            //                       {10, 11, 12}
            //                   };

            //int[,] testArray = {
            //                       {1, 2, 3},
            //                       {4, 5, 6},
            //                       {7, 8, 9},
            //                       {10, 11, 0}
            //                   };

            //int[,] testArray = {
            //                       {1, 2, 3},
            //                       {4, 5, 0},
            //                       {7, 8, 9},
            //                       {10, 11, 0}
            //                   };

            int[,] testArray = {
                                   {1, 2, 3},
                                   {4, 5, 0},
                                   {0, 8, 9},
                                   {10, 11, 0}
                               };
            testArray.Display2DimArray();
            Console.WriteLine();
            Matrix.ZeroMatrix(testArray).Display2DimArray();
            
            Console.ReadLine();
        }
    }
}
