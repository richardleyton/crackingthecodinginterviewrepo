﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._8ZeroMatrix6thEdition
{
    public class Matrix
    {
        // 1.8 in 6th edition, 1.7 in 5th edition.  ZeroMatrix. Write an algorithm such that if an element in an MxN matrix is 0,
        // its entire row and column are set to 0.
        public static int[,] ZeroMatrix(int[,] inputArray)
        {
            int rows = inputArray.GetLength(0);
            int columns = inputArray.GetLength(1);
            bool[] zeroRows = new bool[rows];
            bool[] zeroColumns = new bool[columns];
            int[,] resultArray = (int[,])inputArray.Clone();
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (inputArray[row, column] == 0)
                    {
                        zeroRows[row] = true;
                        zeroColumns[column] = true;
                    }
                }
            }

            for (int row = 0; row < rows; row++)
            {
                if (zeroRows[row])
                {
                    for (int column = 0; column < columns; column++)
                    {
                        resultArray[row, column] = 0;
                    }
                }
            }    

            for (int column = 0; column < columns; column++)
            {
                if (zeroColumns[column])
                {
                    for (int row = 0; row < rows; row++)
                    {
                        resultArray[row, column] = 0;
                    }
                }
            }
            return resultArray;
        }
    }
}
