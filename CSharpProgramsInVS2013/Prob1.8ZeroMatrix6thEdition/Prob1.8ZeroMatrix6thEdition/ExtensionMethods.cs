﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._8ZeroMatrix6thEdition
{
    public static class ExtensionMethods
    {
        public static void Display2DimArray(this Int32[,] myArray)
        {
            int bound0 = myArray.GetUpperBound(0);
            int bound1 = myArray.GetUpperBound(1);
            for (int x = 0; x <= bound0; x++)
            {
                for (int y = 0; y <= bound1; y++)
                {
                    Console.Write("{0}\t", myArray[x, y]);
                }
                Console.WriteLine("");
            }

        }
    }
}
