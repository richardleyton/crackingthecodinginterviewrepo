﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._7Intersection
{
    class Program
    {
        static void Main(string[] args)
        {
            var testList1 = new SinglyLinkedList<string>();
            var testList2 = new SinglyLinkedList<string>();

            var sharedNode = new SinglyLinkedListNode<string>();
            sharedNode.Value = "Shared Node";

            var sharedNode2 = new SinglyLinkedListNode<string>();
            sharedNode2.Value = "Shared Node2";

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst("List2Node4");
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with no intersecting nodes");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, both lists start with sharedNode, 
            //                                                                      followed by only List2Nodes
            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst(sharedNode);

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst("List2Node4");
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst(sharedNode);

            //Console.WriteLine("Test Case with intersecting node at head of both lists");
            //TestIntersections(testList1, testList2);

            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst("List2Node4");
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with intersecting node at tail of both lists");
            //TestIntersections(testList1, testList2);

            //testList1.AddFirst(sharedNode);

            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst(sharedNode);

            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with intersecting node at tail of both lists, shorter for debugging");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, List1 is just sharedNode, List2 is what 
            //                                                                      you would expect

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst(sharedNode);

            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst("List2Node4");
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with intersecting node at head of first list, tail of second list");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, List1 corrupted, basically disappeared or 
            //                                                                      is just part of List2

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst(sharedNode);

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with intersecting node at head of first list, interior of second list");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, List1 is corrupted

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst("List1Node4");
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with intersecting node at interior of both lists");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, infinite list, probably circular.  Infinite 
            //                                                                      loop when displaying lists

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with 2 instances of shared node in both lists");
            //TestIntersections(testList1, testList2);

            //*******************************Invalid test*************************, corrupted List1

            //testList1.AddFirst("List1Node5");
            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst(sharedNode2);
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst("List2Node7");
            //testList2.AddFirst("List2Node6");
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst(sharedNode2);
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst(sharedNode);
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with 2 different nodes in each list");
            //TestIntersections(testList1, testList2);

            testList1.AddFirst(sharedNode2);
            testList1.AddFirst(sharedNode);
            testList1.AddFirst("List1Node3");
            testList1.AddFirst("List1Node2");
            testList1.AddFirst("List1Node1");

            testList2.AddFirst(sharedNode2);
            testList2.AddFirst(sharedNode);
            testList2.AddFirst("List2Node5");
            testList2.AddFirst("List2Node4");
            testList2.AddFirst("List2Node3");
            testList2.AddFirst("List2Node2");
            testList2.AddFirst("List2Node1");

            Console.WriteLine("Test Case add sharedNode => sharedNode2 to end of both lists");
            TestIntersections(testList1, testList2);


            //*******************************Invalid test*************************, sharedNode2 is not added to second list
            //testList1.AddFirst(sharedNode2);
            //testList1.AddFirst(sharedNode);
            //testList1.AddFirst("List1Node3");
            //testList1.AddFirst("List1Node2");
            //testList1.AddFirst("List1Node1");

            //testList2.AddFirst(sharedNode);  // sharedNode.Next is set to head, which is initially null, so sharedNode2 is not
            //                                 // automatically added
            ////testList1.AddFirst(sharedNode2);
            //testList2.AddFirst("List2Node5");
            //testList2.AddFirst("List2Node4");
            //testList2.AddFirst("List2Node3");
            //testList2.AddFirst("List2Node2");
            //testList2.AddFirst("List2Node1");

            //Console.WriteLine("Test Case with sharedNode => sharedNode2 at end of both lists");
            //TestIntersections(testList1, testList2);

            Console.ReadLine();
        }

        static void TestIntersections<T>(SinglyLinkedList<T> List1, SinglyLinkedList<T> List2)
        {
            SinglyLinkedListNode<T> bruteForceResultNode;
            SinglyLinkedListNode<T> dictionaryResultNode;

            SinglyLinkedListNode<T> current;
            Console.WriteLine("List1:");
            current = List1.head;
            while (current != null)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }

            Console.WriteLine("List2:");
            current = List2.head;
            while (current != null)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }

            bruteForceResultNode = SinglyLinkedList<T>.IntersectionBruteForce(List1, List2);
            dictionaryResultNode = SinglyLinkedList<T>.IntersectionWithDictionary(List1, List2);

            if (bruteForceResultNode == null)
            {
                Console.WriteLine("bruteForceResultNode is null.  The lists do not intersect.");
            }
            else
            {
                Console.WriteLine("bruteForceNodeResultNode is {0}.  Value of this node is {1}",
                                  bruteForceResultNode, bruteForceResultNode.Value);
            }

            if (dictionaryResultNode == null)
            {
                Console.WriteLine("dictionaryResultNode is null.  The lists do not intersect.");
            }
            else
            {
                Console.WriteLine("dictionaryResultNode is {0}.  Value of this node is {1}",
                                  dictionaryResultNode, dictionaryResultNode.Value);
            }
        }
    }
}
