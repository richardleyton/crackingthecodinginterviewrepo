﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._7Intersection
{
    public class SinglyLinkedListNode<T>
    {
        public T Value;
        public SinglyLinkedListNode<T> Next;
    }
    public class SinglyLinkedList<T>
    {
        public SinglyLinkedListNode<T> head = null;

        public void AddFirst(T val)
        {
            var newNode = new SinglyLinkedListNode<T>();
            newNode.Value = val;
            newNode.Next = head;
            head = newNode;
        }

        public void AddFirst(SinglyLinkedListNode<T> nodeToAdd)
        {
            nodeToAdd.Next = head;
            head = nodeToAdd;
        }

        public static SinglyLinkedListNode<T> IntersectionBruteForce(SinglyLinkedList<T> List1, SinglyLinkedList<T> List2)
        {
            // If no intersection is found, null is returned

            SinglyLinkedListNode<T> currentNode1 = List1.head;
            //SinglyLinkedListNode<T> currentNode2 = List2.head;
            SinglyLinkedListNode<T> currentNode2;

            while (currentNode1 != null)
            {
                currentNode2 = List2.head;  // ssign currentNode2 here instead of before both loops
                while (currentNode2 != null)
                {
                    //if (currentNode1 == currentNode2)
                    if (ReferenceEquals(currentNode1, currentNode2))
                    {
                        return currentNode1;
                    }
                    currentNode2 = currentNode2.Next;
                }
                currentNode1 = currentNode1.Next;
            }
            return null;
        }

        public static SinglyLinkedListNode<T> IntersectionWithDictionary(SinglyLinkedList<T> List1, SinglyLinkedList<T> List2)
        {
            // If no intersection is found, null is returned

            SinglyLinkedListNode<T> currentNode1 = List1.head;
            SinglyLinkedListNode<T> currentNode2 = List2.head;
            var dict = new Dictionary<SinglyLinkedListNode<T>, SinglyLinkedListNode<T>>();

            while (currentNode1 != null)
            {
                // use d.Add("One", 1); or d["Two"] = 2; notation
                dict[currentNode1] = currentNode1;
                currentNode1 = currentNode1.Next;
            }

            while (currentNode2 != null)
            {
                if (dict.ContainsKey(currentNode2))
                {
                    return currentNode2;
                }
                currentNode2 = currentNode2.Next;
            }
            return null;
        }
    }
}
