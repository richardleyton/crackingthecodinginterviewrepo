﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._6PalAuthorOnlineRecursive
{
    public class HeadAndTail
    {
        public LinkedListNode head;
        public LinkedListNode tail;
        public HeadAndTail(LinkedListNode h, LinkedListNode t)
        {
            head = h;
            tail = t;
        }
    }
}
