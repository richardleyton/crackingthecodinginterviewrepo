﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._6PalAuthorOnlineRecursive
{
    class Program
    {
        static void Main(string[] args)
        {
            var node1 = new LinkedListNode(1);
            var node2 = new LinkedListNode(2);
            var node3 = new LinkedListNode(3);
            var node4 = new LinkedListNode(2);
            var node5 = new LinkedListNode(1);
            //var node4 = new LinkedListNode(4);
            //var node5 = new LinkedListNode(5);

            node1.Next = node2;
            node2.Next = node3;
            node3.Next = node4;
            node4.Next = node5;
            node5.Next = null;

            node1.Prev  = null;
            node2.Prev = node1;
            node3.Prev = node2;
            node4.Prev = node3;
            node5.Prev = node4;

            node1.Last = node5;
            node2.Last = node5;
            node3.Last = node5;
            node4.Last = node5;
            node5.Last = node5;
            //Console.WriteLine("The list is {0}a palindrome", testList.IsListPalindrome() ? "" : "not ");
            //Console.WriteLine(IsPalindrome(testList));
            //Console.WriteLine("The list is {0}a palindrome", IsPalindrome(testList.First) ? "" : "not ");
            Console.WriteLine("The list is {0}a palindrome", IsPalindrome(node1) ? "" : "not ");
            HeadAndTail htToTestReverse = Reverse(node1);

            Console.ReadLine();
        }


        //bool IsPalindrome(LinkedListNode<int> head)
        static bool IsPalindrome(LinkedListNode head)
        {
            var size = 0;
            var node = head;

            while (node != null)
            {
                size++;
                node = node.Next;
            }

            var palindrome = IsPalindromeRecurse(head, size);

            return palindrome.result;
        }
        //Result IsPalindromeRecurse(LinkedListNode<int> head, int length)
        static Result IsPalindromeRecurse(LinkedListNode head, int length)
        {
            if (head == null || length == 0)
            {
                return new Result(null, true);
            }
            else if (length == 1)
            {
                return new Result(head.Next, true);
            }
            else if (length == 2)
            {
                return new Result(head.Next.Next, head.Data == head.Next.Data);
            }

            var res = IsPalindromeRecurse(head.Next, length - 2);

            if (!res.result || res.Node == null)
            {
                return res; // Only "result" member is actually used in the call stack.
            }
            else
            {
                res.result = head.Data == res.Node.Data;
                res.Node = res.Node.Next;
                return res;
            }
        }

        static HeadAndTail Reverse(LinkedListNode head)
        {
            if (head == null)
            {
                return null;
            }
            HeadAndTail ht = Reverse(head.Next);
            LinkedListNode clonedHead = head.Clone();  // Clones the whole list, not just one node.
            clonedHead.Next = null;

            if (ht == null)
            {
                return new HeadAndTail(clonedHead, clonedHead);
            }
            ht.tail.Next = clonedHead;
            return new HeadAndTail(ht.head, clonedHead);
        }
    }
}
