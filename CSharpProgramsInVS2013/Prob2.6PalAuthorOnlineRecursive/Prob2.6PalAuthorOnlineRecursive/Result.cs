﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._6PalAuthorOnlineRecursive
{
    class Result
    {
        public LinkedListNode Node;
        public bool result;

        public Result(LinkedListNode node, bool res)
        {
            Node = node;
            result = res;
        }
    }
}
