﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._2ReturnKthToLast
{
    class Program
    {
        static void Main(string[] args)
        {
            List testList = new List();
            testList.AddFirst(10);
            testList.AddFirst(9);
            testList.AddFirst(8);
            testList.AddFirst(7);
            testList.AddFirst(6);
            testList.AddFirst(5);
            testList.AddFirst(4);
            testList.AddFirst(3);
            testList.AddFirst(2);
            testList.AddFirst(1);

            Console.WriteLine("size of list is {0}", testList.Count);
            testList.printAllNodes();
            Console.WriteLine("Value of {0}th to last node is {1}", 2,
                testList.RetrieveKthToLast(2) == null ? "null" : (testList.RetrieveKthToLast(2).data).ToString());
            Console.WriteLine("Value of {0}th to last node is {1}", 4,
                testList.RetrieveKthToLast(4) == null ? "null" : (testList.RetrieveKthToLast(4).data).ToString());
            Console.WriteLine("Value of {0}th to last node is {1}", 10,
                testList.RetrieveKthToLast(10) == null ? "null" : (testList.RetrieveKthToLast(10).data).ToString());
            Console.WriteLine("Value of {0}th to last node is {1}", 1,
                testList.RetrieveKthToLast(1) == null ? "null" : (testList.RetrieveKthToLast(1).data).ToString());
            Console.WriteLine("Value of {0}th to last node is {1}", 0,
                testList.RetrieveKthToLast(0) == null ? "null" : (testList.RetrieveKthToLast(0).data).ToString());
            Console.WriteLine("Value of {0}th to last node is {1}", 11,
                testList.RetrieveKthToLast(11) == null ? "null" : (testList.RetrieveKthToLast(11).data).ToString());

            Console.ReadLine();

        }
    }
}
