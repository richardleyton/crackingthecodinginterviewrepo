﻿using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._2ReturnKthToLast
{
    public class List
    {
        public class Node
        {
            public Node next = null;
            public int data;

            public Node(int d)
            {
                data = d;
            }

            public Node() { }
        }

        private int size;
        public int Count
        {
            get
            {
                return size;
            }
        }

        /// <summary>
        /// The head of the list.
        /// </summary>
        private Node head;

        public List()
        {
            size = 0;
            head = null;
        }

        public Node RetrieveKthToLast(int kthToLastPosition)
        {
            Node tempNode = head;
            Node retNode = null;
            int count = 0;

            while (tempNode != null)
            {
                if (count == size - kthToLastPosition)
                {
                    retNode = tempNode;
                    break;
                }
                count++;
                tempNode = tempNode.next;
            }

            return retNode;
        }

        public void AddFirst(int data)
        {
            Node toAdd = new Node();

            toAdd.data = data;
            toAdd.next = head;

            head = toAdd;
            size++;
        }
        public void printAllNodes()
        {
            Node current = head;
            while (current != null)
            {
                Console.WriteLine(current.data);
                current = current.next;
            }
        }
    }
}
