﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._4CheckBalanced
{
    class Program
    {
        static void Main(string[] args)
        {

            var node0 = new TreeNode<string>("node0Depth0(Root)");

            var node1Depth1 = new TreeNode<string>("node1Depth1");
            var node2Depth1 = new TreeNode<string>("node2Depth1");

            var node3Depth2 = new TreeNode<string>("node3Depth2");
            var node4Depth2 = new TreeNode<string>("node4Depth2");
            var node5Depth2 = new TreeNode<string>("node5Depth2");
            var node6Depth2 = new TreeNode<string>("node6Depth2");

            var node7Depth3 = new TreeNode<string>("node7Depth3");
            var node8Depth3 = new TreeNode<string>("node8Depth3");
            var node9Depth3 = new TreeNode<string>("node9Depth3");
            var node10Depth3 = new TreeNode<string>("node10Depth3");
            var node11Depth3 = new TreeNode<string>("node11Depth3");
            var node12Depth3 = new TreeNode<string>("node12Depth3");
            var node13Depth3 = new TreeNode<string>("node13Depth3");
            var node14Depth3 = new TreeNode<string>("node14Depth3");

            ////node0.children = new TreeNode<string>[] { node1Depth1, node2Depth1 };
            node0.left = node1Depth1;
            node0.right = node2Depth1;

            ////node1Depth1.children = new TreeNode<string>[] { node3Depth2, node4Depth2 };
            node1Depth1.left = node3Depth2;
            node1Depth1.right = node4Depth2;
            ////node2Depth1.children = new TreeNode<string>[] { node5Depth2, node6Depth2 };
            node2Depth1.left = node5Depth2;
            //node2Depth1.right = node6Depth2;

            ////node3Depth2.children = new TreeNode<string>[] { node7Depth3, node8Depth3 };
            node3Depth2.left = node7Depth3;
            node3Depth2.right = node8Depth3;
            ////node4Depth2.children = new TreeNode<string>[] { node9Depth3, node10Depth3 };
            node4Depth2.left = node9Depth3;
            node4Depth2.right = node10Depth3;
            ////node5Depth2.children = new TreeNode<string>[] { node11Depth3, node12Depth3 };
            node5Depth2.left = node11Depth3;
            node5Depth2.right = node12Depth3;
            ////node6Depth2.children = new TreeNode<string>[] { node13Depth3, node14Depth3 };
            //node6Depth2.left = node13Depth3;
            //node6Depth2.right = node14Depth3;

            // Test with only node0 worked properly and returned balanced.
            // Test with node0, node1Depth1, and node2Depth1 worked properly and returned balanced.
            // Test with node0, node1Depth1 worked properly and returned balanced.
            // Test with null passed to IsBalanced worked properly and returned balanced.
            // Test with all nodes for depth 0, 1, and 2 failed and returned not balanced.
            // Problem fixed by changing the order of the conditional in IsBalanced

            // Test with 2 levels deep, but only left side of tree.  node0, 1, 3, 4.  Leave off n2d1, n5d2, and n6d2.
            // Correct - says not balanced.

            // Test with 2 levels deep, but only right side of tree.  Correct - says not balanced
            // Test 3 levels deep, full tree.  Correct - says balanced
            // Test 3 levels deep, leave off n6D2, n13D3, and n14D3.  Correct - says not balanced
            Console.WriteLine(
                "The binary tree with root {0} is{1} balanced", node0.value, node0.IsBalanced(node0, 0) ? "" : " not");
            //Console.WriteLine(
            //    "The binary tree with root {0} is{1} balanced", node0.value, node0.IsBalanced(null, 0) ? "" : " not");

            Console.ReadLine();
        }
    }
}
