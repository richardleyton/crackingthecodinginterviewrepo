﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._4CheckBalanced
{
    public class TreeNode<T>
    {
        public T value;
        //public TreeNode<T> left = new TreeNode<T>();
        //public TreeNode<T> right = new TreeNode<T>();
        public TreeNode<T> left;
        public TreeNode<T> right;
        public bool marked;      // Also called visited
        public int depth;

        public TreeNode()
        {
        }

        public TreeNode(T v)
        {
            value = v;
        }

        public bool IsBalanced(TreeNode<T> root, int depthOfNode)
        {
            if (root == null)
            {
                return true;
            }          
            if (root.left != null && root.right != null)
            {
                return IsBalanced(root.left, depthOfNode + 1) && IsBalanced(root.right, depthOfNode + 1);
                //return Math.Abs(GetDepthOfTree(root.left) - GetDepthOfTree(root.right)) < 2;      
            }
            else if (root.right == null && root.left == null)
            {
                // both left and right subtrees are null
                return true;
            }
            else if (root.left == null)   
            {
                
                // Depth of left subtree is 0
                return GetDepthOfTree(root.right, depthOfNode + 1) < 2;
            }
            else //if (root.right == null)   
            {
                // Depth of left subtree is 0
                return GetDepthOfTree(root.left, depthOfNode + 1) < 2;
            }
        }

        // The author's version of IsBalanced that is similar to mine, but a little simpler
        public bool IsBalancedAuthors(TreeNode<T> root)
        {
            if (root == null)
            {
                return true; // Base case
            }
            int heightDiff = GetHeight(root.left) - GetHeight(root.right);
            if (Math.Abs(heightDiff) > 1)
            {
                return false;
            }
            else
            {
                // Recurse
                return IsBalancedAuthors(root.left) && IsBalancedAuthors(root.right);
            }
        }

        public int GetDepthOfTree(TreeNode<T> root, int depthOfNode)
        {
            if (root == null)
            {
                return depthOfNode - 1;  // return depth of non-null node
            }
            return Math.Max(GetDepthOfTree(root.left, depthOfNode + 1), GetDepthOfTree(root.right, depthOfNode + 1));
        }

        // The author's version of GetDepthOfTree.  A little simpler.  I like it.
        int GetHeight(TreeNode<T> root)
        {
            if (root == null)
            {
                return 0; // Base case
            }
            return Math.Max(GetHeight(root.left), GetHeight(root.right) + 1);
        }

    }
}
