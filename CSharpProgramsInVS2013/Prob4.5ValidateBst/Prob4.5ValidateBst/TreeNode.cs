﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._5ValidateBst
{
    //public class TreeNode<T>
    public class TreeNode<T> where T : struct, IComparable<T>  // struct is important to make sure that T is a non-nullable type
                                                               // when declaring last_printed
    {
        public T value;
        public TreeNode<T> left;
        public TreeNode<T> right;

        public TreeNode()
        {
        }

        public TreeNode(T v)
        {
            value = v;
        }


        static T? last_printed = null;
        public bool IsBinaryTreeBst(TreeNode<T> root)
        {
            //if (root != null)
            if (root == null)
            {
                return true;
            }
            
            if (!IsBinaryTreeBst(root.left))  // Check / recurse left
            {
                return false;
            }
            //if (!Visit(root))
            //{
            //    return false;
            //}

            // Check current
            //if (last_printed != null && root.value <= last_printed)
            if (last_printed != null && root.value.CompareTo(last_printed.GetValueOrDefault()) <= 0)
            {
                return false;
            }
            last_printed = root.value;

            if (!IsBinaryTreeBst(root.right))  // Check / recurse right
            {
                return false;
            }
            return true;
        }
 
        //public bool Visit(TreeNode<T> root)
        //{
        //    Console.WriteLine("Visited node {0}", root.value);
        //    //if (root.left.value <= root.value && root.value < right.value)
        //    //if (root.left.value.CompareTo(root.value) <= 0 && root.value.CompareTo(right.value) < 0)
        //    if (root.left != null && root.right != null)
        //    {
        //        if (root.left.value.CompareTo(root.value) <= 0 && root.value.CompareTo(root.right.value) < 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        public TreeNode<T> CreateMinimalTree(T[] inputArray)
        {
            int numberOfNodes = inputArray.Length;
            int centerNodeIndex;
            TreeNode<T> centerNode;

            if (numberOfNodes == 1)
            {
                centerNode = new TreeNode<T>(inputArray[0]);
            }
            else if (numberOfNodes == 2)
            {
                centerNode = new TreeNode<T>(inputArray[1]);
                centerNode.left = new TreeNode<T>(inputArray[0]);
            }
            else
            {
                // Handle the center node
                centerNodeIndex = (int)Math.Floor((double)(numberOfNodes / 2));
                centerNode = new TreeNode<T>(inputArray[centerNodeIndex]);

                // Left side of center
                centerNode.left = CreateMinimalTree(inputArray.Take(centerNodeIndex).ToArray<T>());

                // Right side of center
                centerNode.right = CreateMinimalTree(inputArray.Skip(centerNodeIndex + 1).Take(numberOfNodes - centerNodeIndex - 1).ToArray<T>());
            }

            return centerNode;
        }

        public bool IsBst()  // From https://github.com/gaylemcd/ctci/blob/ce3ddfe09f2099cb6c5361849a24467569494398/c-sharp/ctci.Library/TreeNode.cs
        {
            //if (Left != null)
            if (left != null)
            {
                //if (Data < Left.Data || !Left.IsBst())
                //if (value < left.value || !left.IsBst())
                if (value.CompareTo(left.value) < 0 || !left.IsBst())
                {
                    return false;
                }
            }

            //if (Right != null)
            if (right != null)
            {
                //if (Data >= Right.Data || !Right.IsBst())
                //if (value >= right.value || !right.IsBst())
                if (value.CompareTo(right.value) >= 0 || !right.IsBst())
                {
                    return false;
                }
            }

            return true;
        }
    }
}
