﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._5ValidateBst
{
    class Program
    {
        static void Main(string[] args)
        {

            // This test case won't compile since I added the constraint to T to be struct, i.e. non-nullable
            //var node0 = new TreeNode<string>("node0Depth0(Root)");

            //var node1Depth1 = new TreeNode<string>("node1Depth1");
            //var node2Depth1 = new TreeNode<string>("node2Depth1");

            //var node3Depth2 = new TreeNode<string>("node3Depth2");
            //var node4Depth2 = new TreeNode<string>("node4Depth2");
            //var node5Depth2 = new TreeNode<string>("node5Depth2");
            //var node6Depth2 = new TreeNode<string>("node6Depth2");

            //var node7Depth3 = new TreeNode<string>("node7Depth3");
            //var node8Depth3 = new TreeNode<string>("node8Depth3");
            //var node9Depth3 = new TreeNode<string>("node9Depth3");
            //var node10Depth3 = new TreeNode<string>("node10Depth3");
            //var node11Depth3 = new TreeNode<string>("node11Depth3");
            //var node12Depth3 = new TreeNode<string>("node12Depth3");
            //var node13Depth3 = new TreeNode<string>("node13Depth3");
            //var node14Depth3 = new TreeNode<string>("node14Depth3");

            //node0.left = node1Depth1;
            //node0.right = node2Depth1;

            //node1Depth1.left = node3Depth2;
            //node1Depth1.right = node4Depth2;

            //node2Depth1.left = node5Depth2;
            //node2Depth1.right = node6Depth2;

            //node3Depth2.left = node7Depth3;
            //node3Depth2.right = node8Depth3;
            //node4Depth2.left = node9Depth3;
            //node4Depth2.right = node10Depth3;
            //node5Depth2.left = node11Depth3;
            //node5Depth2.right = node12Depth3;
            //node6Depth2.left = node13Depth3;
            //node6Depth2.right = node14Depth3;

            //int arrayLength = 7;
            int arrayLength = 6;
            var testArray = new int[arrayLength];
            var root = new TreeNode<int>();
            //for (int i = 0; i < 7; i++)
            //{
            //    testArray[i] = i;
            //}

            //testArray[0] = 2;
            //testArray[1] = 4;
            //testArray[2] = 6;
            //testArray[3] = 8;
            //testArray[4] = 10;
            //testArray[5] = 20;

            //testArray[0] = 2;
            //testArray[1] = 4;
            //testArray[2] = 6;
            //testArray[3] = 8;
            //testArray[4] = 10;
            //testArray[5] = 2;

            //testArray[0] = 2;
            //testArray[1] = 4;
            //testArray[2] = 6;
            //testArray[3] = 8;
            //testArray[4] = 10;
            //testArray[5] = 3;

            //testArray[0] = 2;
            //testArray[1] = 30;
            //testArray[2] = 6;
            //testArray[3] = 8;
            //testArray[4] = 10;
            //testArray[5] = 20;

            // New version says this is not a valid BST, but it is valid.  This is the inherent problem of this algorithm
            // duplicates.
            // It works for IsBst()
            //var node0 = new TreeNode<int>(20);
            //var node1Depth1 = new TreeNode<int>(20);
            //node0.left = node1Depth1;

            // The following test fails.  This returns that it is a valid BST, but it is not, because it has the same in-order
            // traversal.  If we assume that no duplicate values are allowed, then this is OK (ignored, and won't occur).
            // When I test it with my new version, it properly says that it is not a valid BST.
            // It works for IsBst()
            //var node0 = new TreeNode<int>(20);
            //var node2Depth1 = new TreeNode<int>(20);
            //node0.right = node2Depth1;

            // The following test fails.  This returns that it is a valid BST, but it is not.  25 is in the wrong place.  The
            // condition is that ALL left nodes must be less than or equal to the current node, which must be less than all the
            // right nodes.
            // IsBst() fails for this one.  This returns that it is a valid BST, but it is not.
            var node0 = new TreeNode<int>(20);
            var node1Depth1 = new TreeNode<int>(10);
            var node2Depth1 = new TreeNode<int>(30);
            var node4Depth2 = new TreeNode<int>(25);
            node0.left = node1Depth1;
            node0.right = node2Depth1;
            node1Depth1.right = node4Depth2;


            //root = root.CreateMinimalTree(testArray);

            //Console.WriteLine("Tree is {0}a binary search tree", node0.IsBinaryTreeBst(node0) ? "" : "not ");
            //Console.WriteLine("Tree is {0}a binary search tree", root.IsBinaryTreeBst(root) ? "" : "not ");
            //Console.WriteLine("Tree is {0}a binary search tree", root.IsBst() ? "" : "not ");
            Console.WriteLine("Tree is {0}a binary search tree", node0.IsBst() ? "" : "not ");


            // Tested with 14 TreeNode<string>.  Correctly said not a BST.
            // Added and modified CreatMinimalTree.  Tested with BST created by it.  


            Console.ReadLine();
        }
    }
}
