﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._9StringRotation
{
    class Program
    {
        static void Main(string[] args)
        {
            //string testOriginal = "abcdefg";
            //string testRotated = "abcdefg";

            //string testOriginal = "abcdefgh";
            //string testRotated = "abcdefg";

            //string testOriginal = "abcdefg";
            //string testRotated = "gabcdef";

            //string testOriginal = "abcdefghijklmnopqrstuvwxyz";
            //string testRotated = "ghijklmnopqrstuvwxyzabcdef";

            //string testOriginal = "abcdefzzzjklmnopqrstuvwxyz";
            //string testRotated = "ghijklmnopzzztuvwxyzabcdef";

            //string testOriginal = "1abcdefghijklmnopqrstuvwxyz";
            //string testRotated = "ghijklmnopqrstuvwxyz1abcdef";

            //string testOriginal = "1abcdefghijklmnopqr1stuvwxyz";
            //string testRotated = "ghijklmnopqr1stuvwxyz1abcdef";

            string testOriginal = "1abcdefghijklm1nopqr1stuvwxyz";
            string testRotated = "ghijklm1nopqr1stuvwxyz1abcdef";

            Console.WriteLine("{0} is{1} a rotation of {2}", testOriginal,
                testOriginal.IsRotation(testRotated) ? "" : " not", testRotated);
            Console.ReadLine();
        }
    }
}
