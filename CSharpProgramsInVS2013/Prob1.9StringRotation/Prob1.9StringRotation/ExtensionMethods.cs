﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob1._9StringRotation
{
    public static class ExtensionMethods
    {
        // 1.9 in 6th edition, 1.8 in 5th edition.
        // Assume you have a method isSubstring which checks if one word is a substring of another. Given two strings, s1 and s2,
        // write code to check if s2 is a rotation of s1 using only one call to isSubstring (e.g.,"waterbottle"is a rotation of
        // "erbottlewat")
        public static bool IsRotation(this string s1, string s2)
        {
            // Find first character of s1 in s2 and start matching.  If it matches when wrapping, then it is a rotation.  If not check next place the first char appears to see 
            // if it is a rotation.

            int startingPositionInSecondString = -1;
            string shiftedS2;
            if (s1.Length != s2.Length)
            {
                return false;
            }
 
            for (int i = 0; i < s2.Length; i++)
            {
                if (s1[0] == s2[i])
                {
                    startingPositionInSecondString = i;
                    shiftedS2 = s2.Substring(i, s2.Length - i) + s2.Substring(0, i);
                    //if (isSubString(s1, shiftedS2))
                    if (s1.Contains(shiftedS2))
                    {
                        if (startingPositionInSecondString == 0)
                        {
                            //The string is identical, not a rotation.
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
