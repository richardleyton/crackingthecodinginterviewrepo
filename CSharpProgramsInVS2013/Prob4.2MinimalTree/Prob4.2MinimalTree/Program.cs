﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._2MinimalTree
{
    class Program
    {
        static void Main(string[] args)
        {
            //int arrayLength = 7;
            int arrayLength = 6;
            var testArray = new int[arrayLength];
            var root = new Node<int>();
            //for (int i = 0; i < 7; i++)
            //{
            //    testArray[i] = i;
            //}

            testArray[0] = 2;
            testArray[1] = 4;
            testArray[2] = 6;
            testArray[3] = 8;
            testArray[4] = 10;
            testArray[5] = 20;


            root = root.CreateBinarySearchTree(testArray);

            Console.ReadLine();

        }
    }
}
