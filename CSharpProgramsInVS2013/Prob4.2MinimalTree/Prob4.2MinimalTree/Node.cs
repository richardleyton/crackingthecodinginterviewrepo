﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._2MinimalTree
{
    public class Node<T> where T : IComparable<T>
    {
        public T value;
        public Node<T>[] children = new Node<T>[2];  // Also called adjacent nodes.  Children has only two entries because we are implementing a binary tree. 
        public bool marked;      // Also called visited

        public Node()
        {
        }

        public Node(T v)
        {
            value = v;
        }

        public Node<T> CreateBinarySearchTree(T[] inputArray)
        {
            // Determine minimum height to create a complete binary search tree.

            // int height = Math.Floor(Math.Log(numberOfNodes, 2));

            return CreateMinimalTree(inputArray);

        }

        public Node<T> CreateMinimalTree(T[] inputArray)
        {
            int numberOfNodes = inputArray.Length;
            int centerNodeIndex;
            Node<T> centerNode;

            if (numberOfNodes == 1)
            {
                centerNode = new Node<T>(inputArray[0]);
            }
            else if (numberOfNodes == 2)
            {
                centerNode = new Node<T>(inputArray[1]);
                centerNode.children[0] = new Node<T>(inputArray[0]);
            }
            else
            {
                // Handle the center node
                centerNodeIndex =  (int) Math.Floor((double) (numberOfNodes / 2));
                centerNode = new Node<T>(inputArray[centerNodeIndex]);

                // Left side of center
                centerNode.children[0] = CreateMinimalTree(inputArray.Take(centerNodeIndex).ToArray<T>());

                // Right side of center
                centerNode.children[1] = CreateMinimalTree(inputArray.Skip(centerNodeIndex + 1).Take(numberOfNodes - centerNodeIndex - 1).ToArray<T>());
            }

            return centerNode;
        } 
    }
}
