﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCompression
{
    class Program
    {
        static void Main(string[] args)
        {
            string testString = "aabbb";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "aabbbccccdddddeeeeee";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "eeeeeedddddccccbbbaa";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "aabbbccccdddddeeeeeef";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "abbbccccdddddeeeeeef";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "aab";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "ab";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "abb";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            testString = "aabbbc";
            Console.WriteLine("The string {0} compresses to {1}", testString, testString.CompressString());
            Console.ReadLine();
        }
    }
}
