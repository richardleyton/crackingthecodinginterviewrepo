﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCompression
{
    public static class ExtensionMethods
    {
        // 1.5 Implement a method to perform basic string compression using the counts
        // of repeated characters. For example, the string aabcccccaaa would become
        // a2blc5a3. If the "compressed" string would not become smaller than the original
        // string, your method should return the original string.
        // Started at 7-7 4:40 pm.  Finished in Notepad at 5:25.  Took 45 minutes.

        public static string CompressString(this string str)
        {
            int counter = 0;
            StringBuilder compressedString = new StringBuilder();
            if (String.IsNullOrEmpty(str) || str.Length < 3)
            {
                return str;
            }
            for (int i = 0; i < str.Length; i += counter)
            //for (int i = 1; i < str.Length; i += counter)
            {
                //compressedString.Append(str[i - 1]);
                compressedString.Append(str[i]);
                counter = 1;
                
                //if (str[i] == str[i - 1])
                //if (str[i + 1] == str[i])
                if (i + 1 < str.Length && str[i + 1] == str[i])
                {
                    counter = 2;
                    //for (int j = 1; i +  j < str.Length && str[i + j] == str[i]; j++)
                    for (int j = 1; i + 1 + j < str.Length && str[i + 1 + j] == str[i]; j++)
                    {

                        counter++;
                    }
                    compressedString.Append(counter);
                }
            }
            return compressedString.Length < str.Length ? compressedString.ToString() : str;
        }
    }
}
