﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace Prob1._6RotateImage
{
    // 1.6 Given an image represented by an NxN matrix, where each pixel in the image is
    // 4 bytes, write a method to rotate the image by 90 degrees. Can you do this in
    // place?
    class Program
    {
        static void Main(string[] args)
        {
            //var testArray = new Int32[,]
            //{
            //    {0, 1},
            //    {2, 3}
            //};

            //var testArray = new Int32[,]
            //{
            //    {0, 1, 2},
            //    {3, 4, 5},
            //    {6, 7, 8}
            //};

            //var testArray = new Int32[,]
            //{
            //    {0, 1, 2, 3},
            //    {4, 5, 6, 7},
            //    {8, 9, 10, 11},
            //    {12, 13, 14, 15}
            //};

            //var testArray = new Int32[,]
            //{
            //    {0, 1, 2, 3, 4},
            //    {5, 6, 7, 8, 9},
            //    {10, 11, 12, 13, 14},
            //    {15, 16, 17, 18, 19},
            //    {20, 21, 22, 23, 24}
            //};

            var testArray = new Int32[,]
            {
                {0, 1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10, 11},
                {12, 13, 14, 15, 16, 17},
                {18, 19, 20, 21, 22, 23},
                {24, 25, 26, 27, 28, 29},
                {30, 31, 32, 33, 34, 35}
            };

            Console.WriteLine("Original Pixel Array:");
            testArray.Display2DimArray();

            Console.WriteLine("Pixel Array Rotated 90 Degrees to the right:");

            var stopwatch = Stopwatch.StartNew();

            //RotateImage90Degrees(testArray, true).Display2DimArray();
            RotateImage90DegreesInPlace(testArray, true).Display2DimArray();

            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

            Console.WriteLine("testArray after right rotation:");
            testArray.Display2DimArray();
            //testArray = new Int32[,]
            //{
            //    {0, 1},
            //    {2, 3}
            //};
            //testArray = new Int32[,]
            //{
            //    {0, 1, 2},
            //    {3, 4, 5},
            //    {6, 7, 8}
            //};

            //testArray = new Int32[,]
            //{
            //    {0, 1, 2, 3},
            //    {4, 5, 6, 7},
            //    {8, 9, 10, 11},
            //    {12, 13, 14, 15}
            //};

            //testArray = new Int32[,]
            //{
            //    {0, 1, 2, 3, 4},
            //    {5, 6, 7, 8, 9},
            //    {10, 11, 12, 13, 14},
            //    {15, 16, 17, 18, 19},
            //    {20, 21, 22, 23, 24}
            //};

            testArray = new Int32[,]
            {
                {0, 1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10, 11},
                {12, 13, 14, 15, 16, 17},
                {18, 19, 20, 21, 22, 23},
                {24, 25, 26, 27, 28, 29},
                {30, 31, 32, 33, 34, 35}
            };
            Console.WriteLine("Original Pixel Array:");
            testArray.Display2DimArray();

            Console.WriteLine("Pixel Array Rotated 90 Degrees to the left:");

            stopwatch = Stopwatch.StartNew();

            //RotateImage90Degrees(testArray, false).Display2DimArray();
            RotateImage90DegreesInPlace(testArray, false).Display2DimArray();

            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

            Console.ReadLine();
        }

        public static Int32[,] RotateImage90Degrees(Int32[,] image, bool rotateRight)
        {
            int dimension = image.GetLength(0);
            Int32[,] rotatedImage = new Int32[dimension, dimension];

            if (rotateRight)
            {
                for (int x = 0; x < dimension; x++)
                {
                    for (int y = 0; y < dimension; y++)
                    {
                        rotatedImage[y, dimension - 1 - x] = image[x, y];
                    }
                }
            }
            else
            {
                for (int y = 0; y < dimension; y++)
                {
                    for (int x = 0; x < dimension; x++)
                    {
                        rotatedImage[dimension - 1 - y, x] = image[x, y];
                    }
                }
            }
            return rotatedImage;
        }

        public static Int32[,] RotateImage90DegreesInPlace(Int32[,] image, bool rotateRight)
        {
            int dimension = image.GetLength(0);
            Int32[] source;
            Int32[] topRow = new Int32[dimension];
            Int32[] rightColumn = new Int32[dimension];
            Int32[] bottomRow = new Int32[dimension];
            Int32[] leftColumn = new Int32[dimension];

    
            for (int ringDimension = dimension, originX = 0, originY = 0; ringDimension > 1; ringDimension -= 2, originX++, originY++)
            {
                // Save copy of top row
                for (int y = 0; y < ringDimension; y++)
                {
                    topRow[y] = image[originX + 0, originY + y];
                }


                // Save copy of target right column
                for (int x = 0; x < ringDimension; x++)
                {
                    rightColumn[x] = image[originX + x, originY + ringDimension - 1];
                }
                // Save copy of target bottom row
                for (int y = 0; y < ringDimension; y++)
                {
                    bottomRow[y] = image[originX + ringDimension - 1, originY + y];
                }
                // Save copy of target left column
                for (int x = 0; x < ringDimension; x++)
                {
                    leftColumn[x] = image[originX + x, originY + 0];
                } 
                if (rotateRight)
                {
                    // Cycle through these steps, starting with outer ring of square, then processing inner rings.
                    source = topRow;
                    // Copy source row to target column
                    for (int x = 0; x < ringDimension; x++)
                    {
                        image[originX + x, originY + ringDimension - 1] = source[x];
                    }

                    source = rightColumn;         
                    // Copy source to bottom row, reversing the order
                    // 0th gets ringDimension - 1 - 0, 1st gets ringDimension - 1 - 1, 2nd gets ringDimension - 1 - 2
                    for (int y = 0; y < ringDimension; y++)
                    {
                        image[originX + ringDimension - 1, originY + y] = source[ringDimension - 1 - y];
                    }

                    source = bottomRow;

                    // Copy source to left column
                    for (int x = 0; x < ringDimension; x++)
                    {
                        image[originX + x, originY + 0] = source[x];
                    }

                    source = leftColumn;
                    // Copy source to top row, reversing the order
                    // 0th gets ringDimension - 1 - 0, 1st gets ringDimension - 1 - 1, 2nd gets ringDimension - 1 - 2
                    for (int y = 0; y < ringDimension; y++)
                    {
                        image[originX + 0, originY + y] = source[ringDimension - 1 - y];
                    }
                }
                else
                {
                    // Cycle through these steps, starting with outer ring of square.

                    source = topRow;
                    // Copy source row to target column, 0th or left column, reversing the order
                    // 0th gets ringDimension - 1 - 0, 1st gets ringDimension - 1 - 1, 2nd gets ringDimension - 1 - 2
                    for (int x = 0; x < ringDimension; x++)
                    {
                        image[originX + x, originY + 0] = source[ringDimension - 1 - x];
                    }

                    source = leftColumn;      
                    // Copy source to bottom row
                    for (int y = 0; y < ringDimension; y++)
                    {
                        image[originX + ringDimension - 1, originY + y] = source[y];
                    }

                    source = bottomRow;

                    // Copy source to right column, reversing the order
                    // 0th gets ringDimension - 1 - 0, 1st gets ringDimension - 1 - 1, 2nd gets ringDimension - 1 - 2
                    for (int x = 0; x < ringDimension; x++)
                    {
                        image[originX + x, originY + ringDimension - 1] = source[ringDimension - 1 - x];
                    }

                    source = rightColumn;
                    // Copy source to top row
                    for (int y = 0; y < ringDimension; y++)
                    {
                        image[originX + 0, originY + y] = source[y];
                    }            
                }  // else
            }  // for
            return image;
        }  // public static Int32[,] RotateImage90DegreesInPlace
    }
}
