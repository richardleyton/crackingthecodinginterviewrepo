﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace ReplaceSpaceWithEncoding
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tests = { "One space", "One two three", "One two three four five", "Two  spaces", "Three   spaces   ", 
                                 "NoSpaces", "", null, 
@"Description
 What sets Bank of the West apart from other banks is our team members–they embody the optimistic spirit of the West. There is a spirit here that drives us to do more. Our team of more than 10,000 employees is vital to the success of our Bank. They reflect our modern western values-straightforward, entrepreneurial and optimistic. We seek to create a corporate culture that fosters and rewards excellence, encourages creative thinking and respects diversity – an environment where team members are engaged, supportive of one another and enthusiastic about serving our customers. Bank of the West offers the stability of a company that has a 135 year history and is part of BNP Paribas, a European leader in global banking and financial services and one of the 6 strongest banks in the world. We offer opportunities across our diverse business lines – Retail Banking, Commercial Banking, National Finance, and Wealth Management. 
  
Position Summary: 
The qualified and motivated candidate selected for this position will be responsible for researching and writing technical specifications including problem definition according to IT Development Standards. Code, tests, debugs documents, and maintains programs. Carries out assignments and provides technical guidance to junior members of the unit when requested. Designs systems and writes user procedures. User contact includes system definition, system design, problem resolution, and providing status. Supports major applications with major complexity and scope. May manage multiple projects with multiple cross impact areas. 
" };
            foreach (string test in tests)
            {
                var stopwatch = Stopwatch.StartNew();
                Console.WriteLine("ReplaceSpaceWithPerCent20 test string {0}\nresult {1}",
                                    test, test.ReplaceSpaceWithPerCent20());
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                stopwatch = Stopwatch.StartNew();
                Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayAndLoop test string {0}\nresult {1}",
                                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayAndLoop());
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                stopwatch = Stopwatch.StartNew();
                Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlace test string {0}\nresult {1}",
                                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlace());
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                stopwatch = Stopwatch.StartNew();
                Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced test string {0}\nresult {1}",
                                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced());
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

                stopwatch = Stopwatch.StartNew();
                Console.WriteLine("ReplaceSpaceWithPerCent20UsingGenericList test string {0}\nresult {1}\n",
                                    test, test.ReplaceSpaceWithPerCent20UsingGenericList());
                stopwatch.Stop();
                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

            }

            //test = "One two three";
            //Console.WriteLine("ReplaceSpaceWithPerCent20 test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayAndLoop test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayAndLoop());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlace test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlace());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingGenericList test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingGenericList());

            //test = "One two three four five";
            //Console.WriteLine("ReplaceSpaceWithPerCent20 test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayAndLoop test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayAndLoop());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlace test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlace());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingGenericList test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingGenericList());

            //test = "Two  spaces";
            //Console.WriteLine("ReplaceSpaceWithPerCent20 test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayAndLoop test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayAndLoop());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlace test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlace());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingGenericList test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingGenericList());

            //test = "Three   spaces   ";
            //Console.WriteLine("ReplaceSpaceWithPerCent20 test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayAndLoop test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayAndLoop());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlace test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlace());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced());
            //Console.WriteLine("ReplaceSpaceWithPerCent20UsingGenericList test string {0}\nresult {1}",
            //                    test, test.ReplaceSpaceWithPerCent20UsingGenericList());

            Console.ReadLine();
        }
    }
}
