﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplaceSpaceWithEncoding
{
    public static class ExtensionMethods
    {
        // 1.4 Write a method to replace all spaces in a string with'%20'. You may assume that
        // the string has sufficient space at the end of the string to hold the additional
        // characters, and that you are given the "true" length of the string. (Note: if implementing
        // in Java, please use a character array so that you can perform this operation
        // in place.)
        // EXAMPLE
        // Input: "Mr John Smith
        // Output: "Mr%20Dohn%20Smith"

         //%20 is used for URL encoding of a space, especially in a search string of a query.  So 20 is the 
         //hexadecimal representation of a space.
         //In an ASCII table,
         //Dec	Hx	Oct	Html	Char
         //32	20	040	&#32;	Space

         //As an extension method, make a new class ExtensionMethods.cs and add to project.  Make sure that class          //Extension Methods is declared as public static Extension 
         //Methods.  Do the same for any methods in that class, i.e.bool IsStringPermutationExtensionMethod(this string string1, string string2) should be declared as
         //public static bool IsStringPermutationExtensionMethod(this string string1, string string2).

        public static string ReplaceSpaceWithPerCent20(this string str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return "Test string was null or empty";
            }
            StringBuilder stringStringBuilder= new StringBuilder(str);
            return stringStringBuilder.Replace(" ", "%20").ToString();
        }

        public static string ReplaceSpaceWithPerCent20UsingCharArrayAndLoop(this string str)
        {
            // Use .ToCharArray(), then manipulate array.  Start with last space, so less movement of chars.  Declare array as three             // times length of string so it could handle all spaces.    
    
            //char[] charArray = new char[str.Length * 3]();
            if (String.IsNullOrEmpty(str))
            {
                return "Test string was null or empty";
            }
            char[] charArray = new char[str.Length * 3];
    
            for (int i = 0, arrayIndex = 0; i < str.Length; i++, arrayIndex++)
            {
                if (str[i] != ' ')
                {
                    charArray[arrayIndex] = str[i];
                }
                else
                {
                    charArray[arrayIndex++] = '%';
                    charArray[arrayIndex++] = '2';
                    charArray[arrayIndex] = '0';
            
                }
            }
            return new string(charArray);
        }

        public static string ReplaceSpaceWithPerCent20UsingGenericList(this string str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return "Test string was null or empty";
            }

            List<char> charList = new List<char>();
    
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != ' ')
                {
                    charList.Add(str[i]);
                }
                else
                {
                    charList.Add('%');
                    charList.Add('2');
                    charList.Add('0');
            
                }
            }
            //return charList.ToString();
            return new string(charList.ToArray<char>());
        }

        public static string ReplaceSpaceWithPerCent20UsingCharArrayInPlace(this string str)
        {
            // Use .ToCharArray(), then manipulate array.  Start with last space, looking for spaces from the end of the             // string moving towards the beginning of the string, so less chars will move.  Declare array as three times 
            // length of string so it could handle all spaces.    
    
            if (String.IsNullOrEmpty(str))
            {
                return "Test string was null or empty";
            }

            char[] charArray = new char[str.Length * 3];
            str.ToCharArray().CopyTo(charArray, 0);
            int charsInArray = str.Length;

            for (int i = str.Length - 1; i >= 0; i--)
            {
                // if (str[i] != ' ')
                // {
                //     // Do nothing to charArray
                // }
                if (str[i] == ' ') // Could also use charArray[i] for blank check
                {
                    // slide characters to the right of the blank two places to make room for "%20"
                    for (int j = charsInArray - 1; j > i;  j--)
                    {
                        charArray[j + 2] = charArray[j];
                    }

                    charArray[i] = '%';
                    charArray[i + 1] = '2';
                    charArray[i + 2] = '0';
                    charsInArray += 2;
            
                }
            }
            return new string(charArray);
        }


        public static string ReplaceSpaceWithPerCent20UsingCharArrayInPlaceEnhanced(this string str)
        {
           // Use .ToCharArray(), then manipulate array.  Count how many blanks are in string.  Use this count to determine 
           // size of array needed, saving space over declaring array as 3 times the string length.  Start with last space,            // looking for spaces from the end of the string moving towards the beginning of the string, so less chars will 
           // move.
    
            if (String.IsNullOrEmpty(str))
            {
                return "Test string was null or empty";
            }

            int totalBlanks = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ')
                {
                    totalBlanks++;
                }
            }

            char[] charArray = new char[str.Length + 2 * totalBlanks];
            str.ToCharArray().CopyTo(charArray, 0);
            int indexOfPreviousBlank = str.Length;

 
            int resultStringIndex = str.Length + 2 * totalBlanks - 1;  // index in charArray to move the next char to.

            for (int i = str.Length - 1; i >= 0; i--)
            {
                // if (str[i] != ' ')
                // {
                //     // Do nothing to charArray
                // }
                if (str[i] == ' ') // Could also use charArray[i] for blank check
                {
                    for (int j = indexOfPreviousBlank - i - 1; j > 0; j--)
                    {
                        charArray[resultStringIndex--] = charArray[i + j];
                    }
                    charArray[resultStringIndex--] = '0';
                    charArray[resultStringIndex--] = '2';
                    charArray[resultStringIndex--] = '%';

                    indexOfPreviousBlank = i;
                }
            }
            return new string(charArray);
        }
    }
}
