﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace Prob2._3DeleteMiddleNode
{
    class Program
    {
        static void Main(string[] args)
        {
            var testList = new LinkedList<int>();
            testList.AddFirst(8);
            testList.AddFirst(7);
            testList.AddFirst(6);
            testList.AddFirst(5);
            testList.AddFirst(4);
            testList.AddFirst(3);
            testList.AddFirst(2);
            testList.AddFirst(1);

            testList.Display();

            //Console.WriteLine("Delete first node");
            //testList.RemoveNode(testList.head);

            //testList.Display();

            //Console.WriteLine("Delete second node");
            //testList.RemoveNode(testList.head.Next);

            //testList.Display();

            Console.WriteLine("Delete fourth node");
            testList.RemoveNode(testList.head.Next.Next.Next);

            testList.Display();
            Console.ReadLine();
        }
    }
}
