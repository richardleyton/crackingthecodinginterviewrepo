﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob2._3DeleteMiddleNode
{
    public class LinkedList<T>
    {
        public class Node
        {
            public T data;
            public Node Next;
        }

        public Node head = null;

        public void AddFirst(T nodeContent)
        {
            var newNode = new Node();
            newNode.data = nodeContent;
            newNode.Next = head;
            head = newNode;
        
        }

        public void Display()
        {
            Node current = head;
            while (current != null)
            {
                Console.WriteLine(current.data);
                current = current.Next;
            }
        }

        public void RemoveNode(Node nodeToDelete)
        {
            Node previousNode;
            Node currentNode;

            if (nodeToDelete == null)
            {
                Console.WriteLine("Node to delete was null");
                return;
            }
            if (head == null)
            {
                Console.WriteLine("List was null");
                return;
            }
            if (head == nodeToDelete)
            {
                head = head.Next;
                return;
            }
            previousNode = head;
            currentNode = head.Next;
            while (currentNode != null)
            {
                if (currentNode == nodeToDelete)
                {
                    previousNode.Next = nodeToDelete.Next;
                    return;
                }
                previousNode = currentNode;
                currentNode = currentNode.Next;
            }
        }
    }
}
