﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._3ListOfDepths
{
    public class Node<T>
    {
        public T value;
        public Node<T>[] children = new Node<T>[2];  // Also called adjacent nodes.  Children has only two entries because we are           
                                                     // implementing a binary tree. 
        public bool marked;      // Also called visited
        public int depth;

        public static List<List<Node<T>>> ListOfNodesAtEachDepthResult = new List<List<Node<T>>>();

        public Node()
        {
        }

        public Node(T v)
        {
            value = v;
        }

        public List<List<Node<T>>> ListNodesAtEachDepth(Node<T> root)
        {
            root.DfsTraverse(root);  // Might need to use Node<T>.DfsTraverse(root); instead.  root appears to be OK here.
                                     // Node<T> doesn't show DfsTraverse with Intellisense.

            return ListOfNodesAtEachDepthResult;
        }

        public void DfsTraverse(Node<T> root)
        {
            if (root == null)
            {
                return;
            }
            Visit(root);
            root.marked = true;
            foreach (Node<T> n in root.children)
            {
                if (n != null && n.marked == false)
                {
                    n.depth = root.depth + 1;  // this line was the final addition to fix the program.  Removed all other depth
                                               // incrementing.
                    DfsTraverse(n);
                }
            }

        }

        public void Visit(Node<T> root)
        {
            if (root.depth >= Node<T>.ListOfNodesAtEachDepthResult.Count)
            {
               Node<T>.ListOfNodesAtEachDepthResult.Add(new List<Node<T>>());   // root is at ListOfNodesAtEachDepth[0] for depth 0
            }

            Node<T>.ListOfNodesAtEachDepthResult[root.depth].Add(root);
        }
    }
}
