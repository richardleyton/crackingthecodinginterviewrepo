﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prob4._3ListOfDepths
{
    class Program
    {
        static void Main(string[] args)
        {
            var node0 = new Node<string>("node0Depth0(Root)");

            var node1Depth1 = new Node<string>("node1Depth1");
            var node2Depth1 = new Node<string>("node2Depth1");

            var node3Depth2 = new Node<string>("node3Depth2");
            var node4Depth2 = new Node<string>("node4Depth2");
            var node5Depth2 = new Node<string>("node5Depth2");
            var node6Depth2 = new Node<string>("node6Depth2");

            var node7Depth3 = new Node<string>("node7Depth3");
            var node8Depth3 = new Node<string>("node8Depth3");
            var node9Depth3 = new Node<string>("node9Depth3");
            var node10Depth3 = new Node<string>("node10Depth3");
            var node11Depth3 = new Node<string>("node11Depth3");
            var node12Depth3 = new Node<string>("node12Depth3");
            var node13Depth3 = new Node<string>("node13Depth3");
            var node14Depth3 = new Node<string>("node14Depth3");

            node0.children = new Node<string>[] { node1Depth1, node2Depth1 };

            node1Depth1.children = new Node<string>[] { node3Depth2, node4Depth2 };
            node2Depth1.children = new Node<string>[] { node5Depth2, node6Depth2 };

            node3Depth2.children = new Node<string>[] { node7Depth3, node8Depth3 };
            node4Depth2.children = new Node<string>[] { node9Depth3, node10Depth3 };
            node5Depth2.children = new Node<string>[] { node11Depth3, node12Depth3 };
            node6Depth2.children = new Node<string>[] { node13Depth3, node14Depth3 };

            node0.ListNodesAtEachDepth(node0); 

            List<List<Node<string>>> ListOfNodesAtEachDepth = Node<string>.ListOfNodesAtEachDepthResult;

            Console.ReadLine();
        }
    }
}
