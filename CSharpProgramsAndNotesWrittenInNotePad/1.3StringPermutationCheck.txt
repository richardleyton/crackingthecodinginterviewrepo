// 1.3 Given two strings, write a method to decide if one is a permutation of the other.

public static bool IsStringPermutation(string string1, string string2)
{
    if (String.IsNullOrEmpty(string1) || String.IsNullOrEmpty(string2) || string1.Length != string2.Length)
    {
        return false;
    }
    // Sort both strings and compare.  If they are equal, one is the permutation of the other.
    var string1WithOrderedChars = string1.ToArray().Sort().ToString();
    var string2WithOrderedChars = string2.ToArray().Sort().ToString();
    if (String.StrCompare(string1WithOrderedChars, string2WithOrderedChars) == 0)
    {
        return true;
    }
    return false;
}

As an extension method to the String class:

public static class ExtensionMethods
{
    public static bool IsStringPermutationExtensionMethod(this string string1, string string2)
    {
        if (String.IsNullOrEmpty(string1) || String.IsNullOrEmpty(string2) || string1.Length != string2.Length)
        {
            return false;
        }
        // Sort both strings and compare.  If they are equal, one is the permutation of the other.
        var string1WithOrderedChars = string1.ToArray().Sort().ToString();
        var string2WithOrderedChars = string2.ToArray().Sort().ToString();
        if (String.StrCompare(string1WithOrderedChars, string2WithOrderedChars) == 0)
        {
            return true;
        }
        return false;
    }
}

// Debugging:
// Is there a simple way that I can sort characters in a string in alphabetical order?
// new string (str.OrderBy(c => c).ToArray())
// or
// static string foo3(string str)
// {
//     char[] foo = str.ToArray();
//     Array.Sort(foo);
//     return new string(foo);
// }






http://www.dotnetperls.com/extension

Example. First, here is a custom extension method defined in a program written in the C# language. Generally, you will want to store your extension method class in a separate source file, such as "ExtensionMethods.cs" in your project.

Note:
This file should store a static class with public static extension methods.
Public

Then:
In the rest of your source code, you can invoke these extension methods in the same way as instance methods.

C# program that uses extension method on string

using System;

public static class ExtensionMethods
{
    public static string UppercaseFirstLetter(this string value)
    {
	//
	// Uppercase the first letter in the string.
	//
	if (value.Length > 0)
	{
	    char[] array = value.ToCharArray();
	    array[0] = char.ToUpper(array[0]);
	    return new string(array);
	}
	return value;
    }
}

class Program
{
    static void Main()
    {
	//
	// Use the string extension method on this value.
	//
	string value = "dot net perls";
	value = value.UppercaseFirstLetter();
	Console.WriteLine(value);
    }
}
